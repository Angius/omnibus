<?php

namespace Application\Controller;

use Omnibus\Controller\Controller;

class RssController extends Controller
{
    public function index(): void
    {
        header('Content-Type: application/rss+xml, application/rdf+xml, application/atom+xml, application/xml, text/xml');

        $json = file_get_contents('https://sfnw.online/api/articles.php');

        $data = [
            'items' => json_decode($json, TRUE)
        ];

        $this->render('rss', $data);
    }
}
