<?php


namespace Application\Controller;

use Backend\Models\Article;
use Backend\Models\Category;
use Exception;
use Omnibus\Controller\Controller;

class CategoriesController extends Controller
{
    public function index($params): void
    {
        $articles = [];
        try {
            $articles = empty($params['id']) ? null : Article::GetByCategory($params['id']);
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data = [
            'articles' => $articles,
            'category' => empty($params['id']) ? null : Category::GetByID($params['id']),
        ];

        $this->setBaseData();
        $this->render('categories', $data);
    }
}
