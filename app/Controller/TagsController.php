<?php


namespace Application\Controller;

use Backend\Models\Article;
use Backend\Models\Tag;
use Exception;
use Omnibus\Controller\Controller;

class TagsController extends Controller
{
    public function index($params): void
    {
        $articles = [];
        try {
            $articles = empty($params['id']) ? null : Article::GetByCategory($params['id']);
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data = [
            'articles' => $articles,
            'tag' => empty($params['id']) ? null : Tag::GetByID($params['id']),
        ];

        $this->setBaseData();
        $this->render('tags', $data);
    }
}
