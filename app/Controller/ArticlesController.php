<?php


namespace Application\Controller;

use Backend\Models\Article;
use Exception;
use Omnibus\Controller\Controller;

class ArticlesController extends Controller
{
    public function index($params): void
    {
        $articles = [];
        try {
            $articles = $params['page'] === 'all'
                ? Article::GetAll()
                : Article::GetAll(20, ($params['page'] - 1) * 20);
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data = [
            'articles' => $articles,
        ];

        $this->setBaseData();
        $this->render('articles', $data);
    }
}
