<?php


namespace Application\Controller;

use Backend\Models\Article;
use Backend\Models\User;
use Exception;
use Omnibus\Controller\Controller;

class AuthorsController extends Controller
{
    public function index($params): void
    {
        $articles = [];
        try {
            $articles = empty($params['id']) ? null : Article::GetByAuthor($params['id']);
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data = [
            'articles' => $articles,
            'author' => empty($params['id']) ? null : User::GetByID($params['id']),
        ];

        $this->setBaseData();
        $this->render('authors', $data);
    }
}
