<?php


namespace Application\Controller\User;

use Backend\Models\User;
use Exception;
use Omnibus\Controller\Controller;

/**
 * Class ActivateController
 * @package Application\Controller\User
 */
class ActivateController extends Controller
{
    /**
     * @var bool Tells whether an error occurred or not
     */
    private $err;


    /**
     *
     * @throws Exception
     */
    public function index(): void
    {
        if ($this->session->has('userid')) {
            header('Location: /');
            exit;
        }

        $data = [
            'err'  => $this->err,
        ];

        $this->setBaseData();
        $this->render('user/activate', $data);
    }


    /**
     * @param $params
     * @throws Exception
     */
    public function activate($params): void
    {
        $_code = $_POST['code'] ?? $params['code'] ?? null;
        $this->err = false;
        if (!empty($_code)) {
            $u = User::GetByToken($_code);
            if($u !== null) {
                $u->Activate();
                header('Location: /login?msg=activated');
                die();
            }

            $this->err = 'Incorrect code';
        }

        $this->index();
    }
}
