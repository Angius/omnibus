<?php


namespace Application\Controller\User;

use Backend\Captcha;
use Backend\Models\Database;
use Backend\Models\User;
use Backend\Settings;
use Backend\Token;
use Exception;
use Omnibus\Controller\Controller;
use PDOException;

class RestoreController extends Controller
{
    /**
     * @var array Stores message for the user
     */
    private $message;


    /**
     *
     */
    public function index(): void
    {
        if ($this->session->has('userid')) {
            header('Location: /');
            exit;
        }

        $data = [
            'message' => $this->message,
        ];

        $this->setBaseData();
        $this->render('user/restore', $data);
    }


    /**
     *
     */
    public function restore(): void
    {
        // RESTORE USER
        $error = false;
        $this->message = array(
            'type'   => 'error',
            'header' => 'Error!',
            'body'   => ''
        );
        if (!empty($_POST) && $this->session->has('token')) {

            // Validate tokens
            if ($_POST['token'] !== $this->session->get('token')) {
                die('Access this form from website only.');
            }

            // Check $_POST keys
            $whitelist = array('login', 'email', 'token', 'g-recaptcha-response');
            foreach ($_POST as $key => $item) {
                if (!in_array($key, $whitelist, false)) {
                    die('Please use only the fields in the form.');
                }
            }

            // check secret key
            $captcha = new Captcha(Settings::Read('captcha-secret'));
            $response = $captcha->Verify($_POST['g-recaptcha-response']);

            if ($response === true) {

                // Check if fields aren't empty
                if (empty($_POST['login'])) {
                    $error = true;
                    $this->message['body'] .= '⚠ Empty login<br>';
                }
                if (empty($_POST['email'])) {
                    $error = true;
                    $this->message['body'] .= '⚠ Empty email<br>';
                }

                // Check if email is valid
                if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    $error = true;
                    $this->message['body'] .= '⚠ Invalid email address<br>';
                }

                // Check if user exists
                $l_user = User::GetByName($_POST['login']);
                $e_user = User::GetByEmail($_POST['email']);

                echo '<pre>'.var_export($_POST, true).'</pre>';

                if ($l_user === null || $e_user === null || $e_user->id !== $l_user->id) {
                    $error = true;
                    $this->message['body'] .= '⚠ No such user<br>';
                }

                if (!$error) {

                    // Get user
                    $user = User::GetByEmail($_POST['email']);

                    // Get pass
                    $pass = Token::Get(16);
                    // Encrypt it
                    $safe_pass = password_hash($pass, PASSWORD_BCRYPT);

                    $sql = 'UPDATE `users`
                            SET `password` = :pass
                            WHERE `ID_user` = :id';

                    $sth = Database::Get()->prepare($sql);

                    $sth->bindParam(':id', $user->id);
                    $sth->bindParam(':pass', $safe_pass);

                    try {
                        $sth->execute();
                    } catch (PDOException $e) {
                        $this->message['body'] .= '⚠ Database error<br>';
                    }

                    $body = file_get_contents(dirname(__DIR__, 3).'/public/assets/flatfiles/emails/restore.html');
                    $body = str_replace(array('{{user}}', '{{password}}'), array($_POST['login'], $pass), $body);
                    $subject = 'New password for School for New Writers';
                    $headers = @"From: \"The Dean of SFNW\" <dean@sfnw.online>\r\n" .
                               @"Reply-To: \"The Dean of SFNW\" <dean@sfnw.online>\r\n" .
                               @'X-Mailer: PHP/' . PHP_VERSION .
                               @"MIME-Version: 1.0\r\n" .
                               @"Content-Type: text/html; charset=ISO-8859-1\r\n";

                    try {
                        mail($_POST['email'], $subject, $body, $headers);
                    } catch (Exception $e) {
                        $this->message['body'] .= '⚠ Emailer error<br>';
                    }

                    header('Location: /login?msg=restored');
                }

            } else {
                // redirect if captcha incorrect
                $this->message['body'] .= '⚠ Incorrect Captcha<br>';
            }
        }

        $this->index();
    }
}
