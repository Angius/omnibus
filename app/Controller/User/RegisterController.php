<?php


namespace Application\Controller\User;

use Backend\Models\User;
use Backend\Settings;
use Backend\Captcha;
use Exception;
use Omnibus\Controller\Controller;

/**
 * Class RegisterController
 * @package Application\Controller\User
 */
class RegisterController extends Controller
{

    /**
     * @var array Stores the message to show the user
     */
    private $message;


    /**
     *
     */
    public function index(): void
    {
        if ($this->session->has('userid')) {
            header('Location: /');
            exit;
        }

        $data = [
            'message' => $this->message,
            'privacy' => file_get_contents(dirname(__DIR__, 3).'/public/assets/flatfiles/legal/privacy.md'),
            'terms'   => file_get_contents(dirname(__DIR__, 3).'/public/assets/flatfiles/legal/terms.md'),
        ];

        $this->setBaseData();
        $this->render('user/register', $data);
    }


    /**
     *
     */
    public function register(): void
    {
        $error = false;
        $this->message = array(
            'type'   => 'error',
            'header' => 'Error!',
            'body'   => ''
        );
        if (!empty($_POST) && $this->session->has('token')) {

            // Validate tokens
            if ($_POST['token'] !== $this->session->get('token')) {
                die('Access this form from website only.');
            }

            // Check $_POST keys
            $whitelist = array('login', 'password', 'email', 'token', 'privacy', 'terms', 'g-recaptcha-response');
            foreach ($_POST as $key=>$item) {
                if (!in_array($key, $whitelist, false)) {
                    die('Please use only the fields in the form.');
                }
            }

            // Check Captcha
            $captcha = new Captcha(Settings::Read('captcha-secret'));
            $response = $captcha->Verify($_POST['g-recaptcha-response']);
            if ($response === true) {

                // Check if fields aren't empty
                if (empty($_POST['login'])) {
                    $error = true;
                    $this->message['body'] .= '⚠ Empty login<br>';
                }
                if (empty($_POST['password'])) {
                    $error = true;
                    $this->message['body'] .= '⚠ Empty password<br>';
                }
                if (empty($_POST['email'])) {
                    $error = true;
                    $this->message['body'] .= '⚠ Empty email<br>';
                }

                // Check if emails are valid
                if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    $error = true;
                    $this->message['body'] .= '⚠ Invalid email address<br>';
                }

                // Check if user exists
                if (User::GetByName($_POST['login']) !== null) {
                    $error = true;
                    $this->message['body'] .= '⚠ Name has been taken<br>';
                }
                if (User::GetByEmail($_POST['email']) !== null) {
                    $error = true;
                    $this->message['body'] .= '⚠ Email already exists<br>';
                }

                // If everything's fine, add to database
                if(!$error) {
                    $code = substr(md5(mt_rand()), 0, 15);
                    User::Add($_POST['login'], $_POST['password'], $_POST['email'], $code);

                    $body = file_get_contents(dirname(__DIR__, 3).'/public/assets/flatfiles/emails/welcome.html');
                    $body = str_replace(array('{{user}}', '{{code}}'), array($_POST['login'], $code), $body);
                    $subject = 'School for New Writers activation code';
                    $headers = @"From: \"The Dean of SFNW\" <dean@sfnw.online>\r\n".
                               @"Reply-To: \"The Dean of SFNW\" <dean@sfnw.online>\r\n".
                               @'X-Mailer: PHP/' . PHP_VERSION .
                               @"MIME-Version: 1.0\r\n".
                               @"Content-Type: text/html; charset=ISO-8859-1\r\n";

                    try {
                        mail($_POST['email'], $subject, $body, $headers);
                    } catch (Exception $e) {
                        echo $e;
                    }

                    header('Location: /?msg=registered');
                }

            } else {
                // redirect if captcha incorrect
                $this->message['body'] .= '⚠ Incorrect Captcha<br>';
            }
        }

        $this->index();
    }
}
