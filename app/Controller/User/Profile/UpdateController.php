<?php
namespace Application\Controller\User\Profile;

use Avatar;
use Backend\Models\User;
use Omnibus\Controller\Controller;

/**
 * Class UpdateController
 * @package Application\Controller\User\Profile
 */
class UpdateController extends Controller
{
    /**
     * @var array Stores the message to show the user
     */
    private $message;


    /**
     *
     */
    public function index(): void
    {
        $this->setBaseData();
        $this->render('user/profile/update', []);
    }


    /**
     *
     */
    public function update(): void
    {
        // Message
        $this->message = '';

        // EDIT PROFILE
        if (isset($_POST['type']) && $_POST['token'] === $this->session->get('token')) {
            $u = User::GetByID($_POST['id']);
            switch ($_POST['type']) {
                case 'profile':
                    $path = $_FILES['file']['error'] !== 0 ? null : Avatar::Upload($_POST['id'], $_POST['token'], $_FILES['file']);
                    // Modify user
                    $u->EditProfile($_POST['title'], $_POST['link'], $path);
                    break;

                case 'bio':
                    $u->EditBio($_POST['bio']);
                    break;

                case 'account':
                    if (!password_verify($_POST['password'], $u->password)) {
                        $this->message = 'Current password is incorrect';
                    } else if ($_POST['newpass1'] !== $_POST['newpass2']) {
                        $this->message = 'New passwords do not match';
                    } else {
                        $u->EditAccount($_POST['email'], $_POST['newpass1']);
                    }
                    break;
            }
        }

        header('Location: '.$_SERVER['REQUEST_URI']);
    }
}
