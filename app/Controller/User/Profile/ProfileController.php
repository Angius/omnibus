<?php
namespace Application\Controller\User\Profile;

use Backend\Models\Comment;
use Backend\Models\Shelf;
use Backend\Models\User;
use Exception;
use Omnibus\Controller\Controller;

/**
 * Class ProfileController
 * @package Application\Controller\User\Profile
 */
class ProfileController extends Controller
{
    /**
     *
     * @throws Exception
     */
    public function indexSelf(): void
    {
        $user = User::GetByID($this->session->get('userid'));

        $data = [
            'user'   => $user,
            'self'   => true,
            'counts' => [
                'comments'   => Comment::Count($user->id),
                'bookmarks'  => Shelf::CountBookmarks($user->id),
                'favourites' => Shelf::CountFavourites($user->id)
            ],
        ];

        $this->setBaseData();
        $this->render('user/profile/profile', $data);
    }


    /**
     * @param $params
     * @throws Exception
     */
    public function indexUser($params): void
    {
        $id = $params['id'];
        $user = User::GetByID($id);

        $data = [
            'user'          => User::GetByID($id),
            'counts'        => [
                'comments'      => Comment::Count($user->id),
                'bookmarks'     => Shelf::CountBookmarks($user->id),
                'favourites'    => Shelf::CountFavourites($user->id)
            ],
        ];

        $this->setBaseData();
        $this->render('user/profile/profile', $data);
    }
}
