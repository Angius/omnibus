<?php

namespace Application\Controller\User;

use Backend\Models\User;
use Backend\Settings;
use Exception;
use Omnibus\Controller\Controller;

/**
 * Class LoginController
 * @package Application\Controller\User
 */
class LoginController extends Controller
{
    /**
     * @var array Stores the message to show the user
     */
    private $message;


    /**
     *  Renders the login form
     */
    public function index(): void
    {
        if ($this->session->has('userid')) {
            header('Location: /');
            exit;
        }

        $data = [
            'message' => $this->message,
        ];

        $this->setBaseData();
        $this->render('user/login', $data);
    }


    /**
     * Logs in the user and re-renders the login form if failed
     */
    public function login(): void
    {
        // Get URL data for redirect
        $domain = Settings::Read('domain');

        // LOGIN USER
        $error = false;

        if (!empty($_POST) && $this->session->has('token')) {
            // Validate tokens
            if ($_POST['token'] !== $this->session->get('token')) {
                die('Access this form from website only.');
            }

            // Check $_POST keys
            $whitelist = array('login', 'password', 'remember', 'token');
            foreach ($_POST as $key => $item) {
                if (!in_array($key, $whitelist, false)) {
                    die('Please use only the fields in the form.');
                }
            }

            // Get user
            $user = null;
            if (filter_var($_POST['login'], FILTER_VALIDATE_EMAIL)) {
                $user = User::GetByEmail($_POST['login']);
            } else {
                $user = User::GetByName($_POST['login']);
            }
            echo '<pre>'.var_export('User finding', true).'</pre>';
            echo '<pre>'.var_export($user, true).'</pre>';

            // Check if user really exists
            if (($user !== null) && password_verify($_POST['password'], $user->password)) {

                // Check if user activated
                if ($user->activated) {

                    // Remember user
                    if (isset($_POST['remember'])) {
                        $remember_me = null;
                        try {
                            $remember_me = bin2hex(random_bytes(256));
                        } catch (Exception $e) {
                            $error = true;
                            $this->message = [
                                'type' => 'error',
                                'header' => 'Error!',
                                'body' => $e->getMessage(),
                            ];
                        }
                        if ($remember_me !== null) {
                            User::SetRememberMe($user->id, $remember_me);
                            $cookie = $user->id . ':' . $remember_me;
                            $mac = password_hash($cookie, PASSWORD_BCRYPT);
                            $cookie .= ':' . $mac;
                            if (!IS_DEV) {
                                setcookie('__Secure-rememberme', $cookie, time() + 2592000, '/', 'sfnw.online', true, true);
                            } else {
                                setcookie('__Secure-rememberme', $cookie, time() + 2592000, '/', false, true, true);
                            }
                        }
                    }

                    if (!$error) {
                        $this->session->set('userid', $user->id);
                        header('Location: ' . $domain . '?msg=login');
                        exit;
                    }

                } else {
                    $this->message = [
                        'type' => 'warning',
                        'header' => 'Error!',
                        'body' => 'Your account hasn\'t been activated',
                    ];
                }

            } else {
                $this->message = [
                    'type' => 'error',
                    'header' => 'Error!',
                    'body' => 'Incorrect credentials!',
                ];
            }
        }

//        $this->index();
        echo '<pre>'.var_export($this->session->get('userid'), true).'</pre>';
        echo '<pre>'.var_export($this->session->get('token'), true).'</pre>';
        echo '<pre>'.var_export($_POST, true).'</pre>';
    }


    public function logout(): void
    {
        if (isset($_COOKIE['__Secure-rememberme'])) {
            unset($_COOKIE['__Secure-rememberme']);
            if (!IS_DEV) {
                setcookie('__Secure-rememberme', '', time() - 3600, '/', 'sfnw.online', true, true);
            } else {
                setcookie('__Secure-rememberme', '', time() - 3600, '/', false, true, true);
            }
        }

        // Invalidate session
        $this->session->invalidate();

        // Redirect
        header('Location: ' . Settings::Read('domain') . '?msg=logout');
        die();
    }
}
