<?php
namespace Application\Controller\API;

use Backend\Models\Article;
use Exception;
use Omnibus\Controller\Controller;

class ArticlesSearchController extends Controller
{
    public function index(): void
    {
        $articles = [];
        $error = null;
        try {
            $articles = isset($_GET['limit']) ? Article::Search($_GET['search'], (int)$_GET['limit']) : Article::Search($_GET['search']);
        } catch (Exception $e) {
            $error = 'Database error';
        }

        if (sizeof($articles) <= 0) {
            $error = 'Nothing found';
        }

        $final_json = json_encode([
            'success' => sizeof($articles) > 0,
            'message' => $error,
            'result'  => $articles ?: null
        ]);

        header('Content-Type: application/json');
        echo $final_json;
    }
}
