<?php
namespace Application\Controller\API;

use Backend\Models\Article;
use Exception;
use Omnibus\Controller\Controller;

class ArticleApiController extends Controller
{
    public function index(array $params): void
    {
        $article = null;
        $err = null;

        try {
            $article = Article::GetByID($params['id']);
        } catch (Exception $e) {
            $err = 'Database error';
        }

        if ($article) {
            $final_json = json_encode([
                'success'   => true,
                'error'     => null,
                'result'    => [
                    'id'       => $article->id,
                    'title'    => $article->title,
                    'furl'     => $article->furl,
                    'body'     => $article->body,
                    'date'     => $article->date,
                    'excerpt'  => $article->excerpt,
                    'category' => $article->category,
                    'tags'     => $article->tags,
                    'author'   => [
                        'id'      => $article->author->id,
                        'name'    => $article->author->name,
                        'title'   => $article->author->title,
                        'avatar'  => $article->author->avatar,
                        'role'    => $article->author->role->name
                    ],
                ]
            ]);
        } else {
            $final_json = json_encode([
                'success' => false,
                'error'   => $err ?: 'Article with that ID does not exist',
                'result'  => null
            ]);
        }

        header('Content-Type: application/json');
        echo $final_json;
    }
}
