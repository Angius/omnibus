<?php

namespace Application\Controller\API\Internal;

use Backend\Models\Comment;
use Omnibus\Controller\Controller;

class InternalApiCommentsController extends Controller
{
    public function get(): void
    {
        $comments = Comment::GetAll(true, $_GET['thread']);

        header('Content-Type: text/html');

        foreach ($comments as $c) {

            if ($c->reported_by) {
                $body = '<pre class="reported-comment">pending moderation</pre>';
                $report = '';
            } else {
                $body = $c->body;
                $report = $this->session->has('userid')
                    ? "<div data-tooltip='report' data-id='{$c->id}' class='report-btn'><i class='exclamation triangle icon'></i></div>"
                    : '';
            }

            echo "<div class='ui comment'>";
            echo "<a class='avatar'>";
            echo "<img alt='avatar' class='ui avatar image' src='{$c->author_avatar}'>";
            echo '<a>';
            echo "<div class='content'>";
            echo "<a class='author'>{$c->author}</a>";
            echo "<div class='metadata'>";
            echo "<div class='date s-date'>{$c->date}</div>";
            echo $report;
            echo '</div>';
            echo "<div class='text'>{$body}</div>";
            echo '</div>';
            echo '</div>';
        }
        die();
    }

    public function add(): void
    {
        if ($this->session->get('token') === $_POST['token']) {

            $msg = Comment::Add(
                $_POST['body'],
                $this->session->get('userid'),
                $_POST['thread']
            );

        } else {
            header('HTTP/2.0 500 Forbidden');
            die(json_encode([
                'msg' => 'Cross-server request detected',
            ]));
        }

        header('HTTP/2.0 200 OK');
        header('Content-Type: application/json');
        die(json_encode([
            'msg' => $msg,
        ]));
    }

    public function report(): void
    {
        if ($this->session->get('token') === $_POST['token']) {

            $msg = Comment::Report($_POST['comment'], $this->session->get('userid'));

        } else {
            header('HTTP/2.0 500 Forbidden');
            die(json_encode([
                'msg' => 'Cross-server request detected',
            ]));
        }

        header('HTTP/2.0 200 OK');
        header('Content-Type: application/json');
        die(json_encode([
            'msg' => $msg,
        ]));
    }

}
