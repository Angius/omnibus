<?php
namespace Application\Controller\API\Internal;

use Backend\Models\Shelf;
use Omnibus\Controller\Controller;

class InternalApiShelvesController extends Controller
{
    public function index(): void
    {
        if ($this->session->get('token') === $_POST['token']) {

            $user_id = $this->session->get('userid');
            if($_POST['shelf'] === 'f') {
                $item = 'favourite';
                $msg = self::handleFavourite($_POST['article'], $user_id);
            } else if ($_POST['shelf'] === 'b') {
                $item = 'bookmark';
                $msg = self::handleBookmark($_POST['article'], $user_id);
            } else {
                $item = '';
                $msg = 'No such shelf';
            }
        } else {
            header('HTTP/2.0 500 Forbidden');
            die(json_encode([
                'msg' => 'Cross-server request detected',
            ]));
        }

        header('HTTP/2.0 200 OK');
        header('Content-Type: application/json');
        die(json_encode([
            'item' => $item,
            'msg' => $msg,
        ]));
    }

    private static function handleBookmark(int $article, int $user): string {
        $marked = Shelf::CheckBookmark($article, $user);
        if ($marked) {
            Shelf::RemoveBookmark($article, $user);
            $msg = 'removed';
        } else {
            Shelf::AddBookmark($article, $user);
            $msg = 'added';
        }
        return $msg;
    }

    private static function handleFavourite(int $article, int $user): string {
        $marked = Shelf::CheckFavourite($article, $user);
        if ($marked) {
            Shelf::RemoveFavourite($article, $user);
            $msg = 'removed';
        } else {
            Shelf::AddFavourite($article, $user);
            $msg = 'added';
        }
        return $msg;
    }
}
