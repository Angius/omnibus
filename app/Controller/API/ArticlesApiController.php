<?php
namespace Application\Controller\API;

use Backend\Models\Article;
use Exception;
use Omnibus\Controller\Controller;

class ArticlesApiController extends Controller
{
    public function index(): void {
        $limit = empty($_GET['limit']) ? 0 : $_GET['limit'];

        $articles = [];
        $error = null;
        if (!empty($_GET['category'])) {
            try {
                $articles = Article::GetByCategory($_GET['category'], $limit);
            } catch (Exception $e) {
                $error = 'Database error';
            }
        } else if (!empty($_GET['tag'])) {
            try {
                $articles = Article::GetByTag($_GET['tag'], $limit);
            } catch (Exception $e) {
                $error = 'Database error';
            }
        } else if (!empty($_GET['author'])) {
            try {
                $articles = Article::GetByAuthor($_GET['author'], $limit);
            } catch (Exception $e) {
                $error = 'Database error';
            }
        } else {
            try {
                $articles = Article::GetAll($limit);
            } catch (Exception $e) {
                $error = 'Database error';
            }
        }

        if (sizeof($articles) <= 0) {
            $error = 'Nothing found';
        }

        $final_json = json_encode([
            'success' => sizeof($articles) > 0,
            'message' => $error,
            'result'  => $this->prepare($articles) ?: null
        ]);

        header('Content-Type: application/json');
        echo $final_json;
    }

    private function prepare(array $articles): array
    {
        $arts = [];
        foreach ($articles as $a) {
            $arts[] = [
                'id'       => $a->id,
                'title'    => $a->title,
                'furl'     => $a->furl,
                'body'     => $a->body,
                'date'     => $a->date,
                'excerpt'  => $a->excerpt,
                'category' => $a->category,
                'tags'     => $a->tags,
                'author'   => [
                    'id'      => $a->author->id,
                    'name'    => $a->author->name,
                    'title'   => $a->author->title,
                    'avatar'  => $a->author->avatar,
                    'role'    => $a->author->role->name
                ],
            ];
        }
        return $arts;
    }
}
