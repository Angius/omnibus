<?php


namespace Application\Controller;

use Backend\Models\Article;
use Exception;
use Omnibus\Controller\Controller;

class ArticleController extends Controller
{
    public function index($params): void
    {
        try {
            $data = [
                'article' => Article::GetByID($params['id']),
            ];
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $this->setBaseData();
        $this->render('article', $data);
    }
}
