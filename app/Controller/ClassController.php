<?php


namespace Application\Controller;

use Backend\Models\Classroom;
use Omnibus\Controller\Controller;

class ClassController extends Controller
{
    public function index($params): void
    {
        $data = [
            'class' => empty($params['id']) ? Classroom::GetLatest() : Classroom::GetByID($params['id']),
        ];

        $this->setBaseData();
        $this->render('class', $data);
    }
}
