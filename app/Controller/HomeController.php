<?php

namespace Application\Controller;

use Backend\Models\Article;
use Backend\Models\Quote;
use Exception;
use Omnibus\Controller\Controller;

class HomeController extends Controller
{
    /**
     * @throws Exception
     */
    public function index(): void
    {
        $message = array();
        if (isset($_GET['msg'])) {
            switch ($_GET['msg']) {
                case 'login':
                    $message = array(
                        'type' => 'success timed',
                        'header' => 'Hello, ' . $this->getUser()->name . '!',
                        'body' => 'You have been logged in successfully',
                    );
                    break;
                case 'logout':
                    $message = array(
                        'type' => 'success timed',
                        'header' => 'See you soon!',
                        'body' => 'You have been logged out successfully',
                    );
                    break;
                case 'registered':
                    $message = array(
                        'type' => 'success',
                        'header' => 'Glad you\'re with us!',
                        'body' => 'Your account has been created successfully!<br>Now you only need to <a href="/activate">activate it</a> – expect an email shortly.',
                    );
                    break;

            }
        }

        $articles = [];
        try {
            $articles = Article::GetAll(10);
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data = [
            'quote' => Quote::GetRandomQuote(),
            'message' => $message,
            'articles' => $articles,
        ];

        $this->setBaseData();
        $this->render('home', $data);
    }
}
