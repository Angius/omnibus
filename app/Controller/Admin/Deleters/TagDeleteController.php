<?php
namespace Application\Controller\Admin\Deleters;

use Backend\Models\Tag;
use Exception;
use Omnibus\Controller\Controller;

class TagDeleteController extends Controller
{
    private $message;

    public function index(): void
    {
        if(!$this->getUser()->role->is_admin) {
            die('Unauthorized access');
        }

        $message = null;
        if (isset($_GET['del'], $_GET['t']) && !empty($_GET['del']) && !empty($_GET['t'])) {
            // Check CSRF
            if ($_GET['t'] === $this->session->get('token')) {
                try {
                    Tag::GetByID($_GET['del'])->Delete();
                } catch (Exception $e) {
                    $this->message = 'An issue with deletion has occurred';
                }
                $this->message = 'Tag successfully deleted';
            } else {
                die('Unauthorized access');
            }
        } else {
            $this->message = 'Something\'s empty... '.$_GET['del'].' '.$_GET['t'];
        }

        header('Location: /admin/tags?msg='.$this->message);
    }
}
