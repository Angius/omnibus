<?php

namespace Application\Controller\Admin;

use Backend\Models\Article;
use Backend\Models\Category;
use Backend\Models\Classroom;
use Backend\Models\Comment;
use Backend\Models\Tag;
use Backend\Models\Uptime;
use Backend\Models\User;
use Omnibus\Controller\Controller;

class DashboardController extends Controller
{
    public function index(): void
    {
        // Set up variables
        $counters = [
            'users' => User::Count(),
            'comments' => Comment::Count(),
            'articles' => Article::Count(),
            'categories' => Category::Count(),
            'tags' => Tag::Count(),
            'classes' => Classroom::Count()
        ];

        $uptime = [
            'bot' => Uptime::GetUptime('discord-bot'),
            'omnibus' => Uptime::GetUptime('omnibus')
        ];
        $downtime = [
            'bot' => Uptime::GetDowntime('discord-bot'),
            'omnibus' => Uptime::GetDowntime('omnibus')
        ];
        $status = [
            'bot' => Uptime::GetStatus('discord-bot'),
            'omnibus' => Uptime::GetStatus('omnibus')
        ];
        $logs = [
            'bot' => Uptime::GetLogs('discord-bot'),
            'omnibus' => Uptime::GetLogs('omnibus')
        ];

        // Render Twig template
        $data = [
            'counter' => $counters,

            'downtime' => $downtime,
            'uptime' => $uptime,
            'status' => $status,
            'logs' => $logs,

            'cron' => self::getCronLogs(),
        ];


        $this->setBaseData();
        $this->render('admin/dashboard', $data);
    }

    private static function getCronLogs(): array
    {
        $cron_log = [];
        if (($handle = fopen('cron/log.txt', 'rb')) !== FALSE) {
            foreach (array_filter(explode("\r\n----\r\n", fread($handle, filesize('cron/log.txt')))) as $event) {
                $e = array_filter(explode("\r\n", $event));
                $cron_log[] = [
                    'requested' => $e[0],
                    'generated' => $e[1] ?? 'Processing...'
                ];
            }
        }
        return $cron_log;
    }
}
