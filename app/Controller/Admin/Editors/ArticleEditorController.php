<?php

namespace Application\Controller\Admin\Editors;

use Backend\Models\Article;
use Backend\Models\Tag;
use Backend\Models\User;
use Exception;
use Omnibus\Controller\Controller;

class ArticleEditorController extends Controller
{
    private $message;

    public function index(array $params): void
    {
        $id = isset($params['id']) || array_key_exists('id', $params) ? $params['id'] : null;

        if (!$this->getUser()->role->is_admin) {
            header('Location: /');
        }

        $data = [];
        try {
            $data = [
                'users' => User::GetAllByRole(true, true, true),
                'article' => isset($id) ? Article::GetByID($params['id']) : null,
                'tag_names' => isset($id) ? array_column(Tag::GetByArticle($params['id']), 'name') : null,
                'message' => $this->message,
            ];
        } catch (Exception $e) {
            $this->message = $e->getMessage();
        }

        try {
            $this->setBaseData();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $this->render('admin/editors/article', $data);
    }


    public function add(): void
    {
        if (isset($_POST['token']) && !empty($_POST['token'])) {
            // Check CSRF
            if ($_POST['token'] === $this->session->get('token')) {
                if (!empty($_POST['id']) && isset($_POST['id'])) {

                    try {
                        $art = Article::GetByID($_POST['id'])
                            ->setTitle($_POST['title'])
                            ->setFurl(self::friendlify($_POST['title']))
                            ->setBody($_POST['body'])
                            ->setDate($_POST['date'])
                            ->setExcerpt($_POST['excerpt'])
                            ->setCategory($_POST['category'])
                            ->setTags($_POST['tags'])
                            ->setAuthor($_POST['author']);
                        $art->Edit();
                    } catch (Exception $e) {
                        $this->message = $e->getMessage();
                    }

                } else {

                    $art = new Article();
                    try {
                        $art->setTitle($_POST['title'])
                            ->setFurl(self::friendlify($_POST['title']))
                            ->setBody($_POST['body'])
                            ->setDate($_POST['date'])
                            ->setExcerpt($_POST['excerpt'])
                            ->setCategory($_POST['category'])
                            ->setTags($_POST['tags'])
                            ->setAuthor($_POST['author'])
                            ->Add();
                    } catch (Exception $e) {
                        $this->message = $e->getMessage();
                    }

                }
            } else {
                die('Unauthorized access');
            }

            $this->index(['id' => $_POST['id']]);
        }
    }


    private static function friendlify(string $str): string
    {
        $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
        return $str;
    }
}
