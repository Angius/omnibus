<?php

namespace Application\Controller\Admin\Editors;

use Backend\Models\Role;
use Backend\Models\User;
use Exception;
use Omnibus\Controller\Controller;

class UserEditorController extends Controller
{
    private $message;

    public function index(array $params): void
    {
        $id = isset($params['id']) || array_key_exists('id', $params) ? $params['id'] : null;

        if (!$this->getUser()->role->is_admin) {
            header('Location: /');
        }

        $data = [];
        try {
            $data = [
                'e_user'  => $id ? User::GetByID($id) : null,
                'roles'   => Role::GetLower($this->getUser()->role->rank),
                'message' => $this->message,
            ];
        } catch (Exception $e) {
            $this->message = $e->getMessage();
        }

        try {
            $this->setBaseData();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $this->render('admin/editors/user', $data);
    }


    public function add(): void
    {
        if (isset($_POST['token']) && !empty($_POST['token'])) {
            // Check CSRF
            if ($_POST['token'] === $this->session->get('token')) {
                if (!empty($_POST['id']) && isset($_POST['id'])) {

                    try {
                        $user = User::GetByID($_POST['id'])
                             ->setName($_POST['login'])
                             ->setPassword($_POST['pass'], true)
                             ->setEmail($_POST['email'])
                             ->setLink($_POST['link'])
                             ->setAvatar($_POST['avatar'])
                             ->setTitle($_POST['title'])
                             ->setBio($_POST['bio'])
                             ->setRole($_POST['role']);
                        $user->Edit();
                    } catch (Exception $e) {
                    $this->message = $e->getMessage();
                }

                } else {
                    $user = new User();
                    $user->setName($_POST['login'])
                         ->setPassword($_POST['pass'], true)
                         ->setEmail($_POST['email'])
                         ->setLink($_POST['link'])
                         ->setAvatar($_POST['avatar'])
                         ->setTitle($_POST['title'])
                         ->setBio($_POST['bio'])
                         ->setRole($_POST['role']);
                    $user->Add();
                }
            } else {
                die('Unauthorized access');
            }

            $this->index(['id' => $_POST['id']]);
        }
    }


    private static function friendlify(string $str): string
    {
        $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
        return $str;
    }
}
