<?php
namespace Application\Controller\Admin\Editors;

use Backend\Models\Tag;
use Exception;
use Omnibus\Controller\Controller;

class TagEditorController extends Controller
{
    private $message;

    public function index(array $params): void
    {
        $id = isset($params['id']) || array_key_exists('id', $params) ? $params['id'] : null;

        if (!$this->getUser()->role->is_admin) {
            header('Location: /');
        }

        $data = [];
        try {
            $data = [
                'tag' => $id ? Tag::GetByID($id) : null,
                'message'  => $this->message,
            ];
        } catch (Exception $e) {
            $this->message = $e->getMessage();
        }

        try {
            $this->setBaseData();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $this->render('admin/editors/tag', $data);

        echo '<pre>'.var_export('11111'.$this->message, true).'</pre>';
    }


    public function add(): void
    {
        if (isset($_POST['token']) && !empty($_POST['token'])) {
            // Check CSRF
            if ($_POST['token'] === $this->session->get('token')) {
                if (!empty($_POST['id']) && isset($_POST['id'])) {

                    try {
                        $cat = Tag::GetByID($_POST['id'])
                            ->setName($_POST['name'])
                            ->setFurl(self::friendlify($_POST['name']))
                            ->setDescription($_POST['description'])
                            ->setPicture($_POST['image']);
                        $cat->Edit();
                    } catch (Exception $e) {
                        $this->message = $e->getMessage();
                    }

                } else {

                    $cat = new Tag();
                    $cat->setName($_POST['name'])
                        ->setFurl(self::friendlify($_POST['name']))
                        ->setDescription($_POST['description'])
                        ->setPicture($_POST['image']);
                    $cat->Add();

                }
            } else {
                die('Unauthorized access');
            }

            $this->index(['id' => $_POST['id']]);
        }
    }


    private static function friendlify(string $str): string
    {
        $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
        return $str;
    }
}
