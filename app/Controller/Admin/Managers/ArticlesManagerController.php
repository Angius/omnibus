<?php
namespace Application\Controller\Admin\Managers;

use Backend\Models\Article;
use Exception;
use Omnibus\Controller\Controller;

class ArticlesManagerController extends Controller
{
    private $message;

    public function index(): void
    {
        if (!$this->getUser()->role->is_admin) {
            header('Location: /');
        }

        $articles = [];
        try {
            $articles = Article::GetAll();
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data = [
            'articles' => $articles,
            'message'  => $this->message
        ];

        $this->setBaseData();
        $this->render('admin/managers/articles', $data);
    }

}
