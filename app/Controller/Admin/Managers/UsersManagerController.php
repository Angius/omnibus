<?php
namespace Application\Controller\Admin\Managers;

use Backend\Models\User;
use Exception;
use Omnibus\Controller\Controller;

class UsersManagerController extends Controller
{
    private $message;

    public function index(): void
    {
        if (!$this->getUser()->role->is_admin) {
            header('Location: /');
        }

        $users = [];
        try {
            $users = User::GetAll();
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data = [
            'users' => $users,
            'message'  => $this->message
        ];

        $this->setBaseData();
        $this->render('admin/managers/users', $data);
    }
}
