<?php
namespace Application\Controller\Admin\Managers;

use Backend\Models\Tag;
use Exception;
use Omnibus\Controller\Controller;

class TagsManagerController extends Controller
{
    private $message;

    public function index(): void
    {
        if (!$this->getUser()->role->is_admin) {
            header('Location: /');
        }

        $tags = [];
        try {
            $tags = Tag::GetAll();
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data = [
            'tags' => $tags,
            'message'  => $this->message
        ];

        $this->setBaseData();
        $this->render('admin/managers/tags', $data);
    }
}
