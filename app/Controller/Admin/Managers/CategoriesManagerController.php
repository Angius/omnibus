<?php
namespace Application\Controller\Admin\Managers;

use Backend\Models\Category;
use Exception;
use Omnibus\Controller\Controller;

class CategoriesManagerController extends Controller
{
    private $message;

    public function index(): void
    {
        if (!$this->getUser()->role->is_admin) {
            header('Location: /');
        }

        $categories = [];
        try {
            $categories = Category::GetAll();
        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data = [
            'categories' => $categories,
            'message'  => $this->message
        ];

        $this->setBaseData();
        $this->render('admin/managers/categories', $data);
    }
}
