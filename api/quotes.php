<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 15.08.2018
 * Time: 05:44
 */

use Backend\Models\Quote;

require_once '../backend/models/Quote.php';

$quote = Quote::GetRandomQuote();

header('Content-Type: application/json');
echo json_encode($quote);
