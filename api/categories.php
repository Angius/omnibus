<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 03.09.2018
 * Time: 00:57
 */

use Backend\Models\Category;

require_once dirname(__DIR__) . '/backend/models/Category.php';

if (!empty($_GET['id'])) {

    $category = Category::GetByID($_GET['id']);

    header('Content-Type: application/json');
    echo json_encode($category);

} else if (!empty($_GET['search'])) {

    $categories = isset($_GET['limit']) ? Category::Search($_GET['search'], (int)$_GET['limit']) : Category::Search($_GET['search']);

    header('Content-Type: application/json');

    /** TODO: Pick one way of delivering the API only! */
    if (isset($_GET['dirty'])) {
        echo json_encode($categories);
    } else {
        echo json_encode(array('results' => $categories));
    }

} else {

    $categories = Category::GetAll();

    header('Content-Type: application/json');
    echo json_encode($categories);
}
