<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 03.09.2018
 * Time: 00:57
 */

use Backend\Models\Tag;

require_once dirname(__DIR__) . '/backend/models/Tag.php';

if (!empty($_GET['id'])) {

    $tag = Tag::GetByID($_GET['id']);

    header('Content-Type: application/json');
    echo json_encode($tag);

} else if (!empty($_GET['search'])) {

    $categories = isset($_GET['limit']) ? Tag::Search($_GET['search'], (int)$_GET['limit']) : Tag::Search($_GET['search']);

    header('Content-Type: application/json');

    /** TODO: Pick one way of delivering the API only! */
    if (isset($_GET['dirty'])) {
        echo json_encode($categories);
    } else {
        echo json_encode(array('results' => $categories));
    }

} else {
    $limit = empty($_GET['limit']) ? 0 : $_GET['limit'];
    $tags = Tag::GetAll($limit);

    header('Content-Type: application/json');
    echo json_encode($tags);
}
