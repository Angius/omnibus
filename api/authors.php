<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 03.09.2018
 * Time: 00:58
 */

use Backend\Models\User;

require_once dirname(__DIR__) . '/backend/models/User.php';
require_once dirname(__DIR__) . '/backend/models/Role.php';


/**
 * @param $user
 * @return array
 */
function assemble_user($user) {
    $u = array();
    $u['id']        = $user->id;
    $u['name']      = $user->name;
    $u['bio']       = $user->bio;
    $u['title']     = $user->title;
    $u['link']      = $user->link;
    $u['avatar']    = $user->avatar;
    $u['joined']    = $user->joined;
    $u['last seen'] = $user->last_seen;
    $u['role']      = $user->role->name;
    return $u;
}

/**
 * Get single user
 * ?id=0
 */
if (!empty($_GET['id'])) {

    $user = assemble_user(User::GetByID($_GET['id']));

    header('Content-Type: application/json');
    echo json_encode($user);

/**
 * Search for user by name
 * ?search=string
 * &dirty
 */
} else if (!empty($_GET['search'])) {

    $categories = isset($_GET['limit']) ? User::Search($_GET['search'], (int)$_GET['limit']) : User::Search($_GET['search']);

    header('Content-Type: application/json');

    /** TODO: Pick one way of delivering the API only! */
    if (isset($_GET['dirty'])) {
        echo json_encode($categories);
    } else {
        echo json_encode(array('results' => $categories));
    }

} else {

    $users = User::GetAllByRole(false, true, true);
    $u = array_map('assemble_user', $users);

    header('Content-Type: application/json');
    echo json_encode($u);
}
