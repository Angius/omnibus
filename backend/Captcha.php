<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 29.09.2018
 * Time: 04:51
 */

namespace Backend;

class Captcha
{
    private $_secret;

    /**
     * Captcha constructor.
     * @param $secret
     */
    public function __construct($secret)
    {
        $this->_secret = $secret;
    }

    public function Verify($response) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'secret' => $this->_secret,
            'response' => $response,
            'remoteip' => $_SERVER['REMOTE_ADDR']
        ));

        $resp = json_decode(curl_exec($ch), false);
        curl_close($ch);

        if ($resp->success) {
            return true;
        }

        return json_decode(json_encode($resp), true)['error-codes'];
    }
}
