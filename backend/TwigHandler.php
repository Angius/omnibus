<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 02.11.2018
 * Time: 22:36
 */

namespace Backend;

use Backend\Models\Shelf;
use ParsedownExtra;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig_Extensions_Extension_Date;

class TwigHandler
{
    /**
     * @var array
     */
    private static $socials = [
        'twitter.com'         => ['name' => 'Twitter',    'icon' => 'https://sfnw.online/public/assets/icons/twitter.png'],
        'tumblr.com'          => ['name' => 'Tumblr',     'icon' => 'https://sfnw.online/public/assets/icons/tumblr.png'],
        'fimfiction.net'      => ['name' => 'FiMFiction', 'icon' => 'https://sfnw.online/public/assets/icons/fimfiction.png'],
        'archiveofourown.org' => ['name' => 'AO3',        'icon' => 'https://sfnw.online/public/assets/icons/ao3.png'],
        'fanfiction.net'      => ['name' => 'Fanfiction', 'icon' => 'https://sfnw.online/public/assets/icons/fanfiction.png'],
    ];

    public static function Get(): Environment
    {
        $loader = new FilesystemLoader([
                dirname(__DIR__) . '/public/views',
                dirname(__DIR__) . '/public/assets'
            ]);
        $twig = new Environment($loader);

        $twig->addFilter(new TwigFilter('md', static function (?string $string): ?string {
            $Extra = new ParsedownExtra();
            return $string ? $Extra->parse($string) : null;
        }));

        $twig->addExtension(new Twig_Extensions_Extension_Date());

        $twig->addFunction(new TwigFunction('get_icon', static function (string $url): ?string {
            if ($url === '' || empty($url)) { return null; }

            $host = str_replace('www.', '', parse_url($url, PHP_URL_HOST));

            return TwigHandler::$socials[$host]['icon'] ?? 'https://sfnw.online/public/assets/icons/link.png';
        }));

        $twig->addFunction(new TwigFunction('get_name', static function (string $url): ?string {
            if ($url === '' || empty($url)) { return null; }

            $host = str_replace('www.', '', parse_url($url, PHP_URL_HOST));

            return TwigHandler::$socials[$host]['name'] ?? 'Link';
        }));

        $twig->addFunction(new TwigFunction('check_bookmark', static function (int $article, ?int $user): bool {
            if ($user !== null) {
                return Shelf::CheckBookmark($article, $user);
            }
            return false;
        }));

        $twig->addFunction(new TwigFunction('check_favourite', static function (int $article, ?int $user): bool {
            if ($user !== null) {
                return Shelf::CheckFavourite($article, $user);
            }
            return false;
        }));

        $twig->addFunction(new TwigFunction('asset', static function (string $asset): string {
            return sprintf('/public/%s', ltrim($asset, '/'));
        }));

        return $twig;
    }
}
