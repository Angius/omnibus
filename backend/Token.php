<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 29.09.2018
 * Time: 01:27
 */

namespace Backend;

use Exception;
use RuntimeException;

class Token
{
    /**
     * @param $min int Minimum integer value
     * @param $max int Maximum integer value
     * @return int Returns cryptographically-secure random integer in range
     */
    protected static function crypto_rand_secure($min, $max): int
    {
        $range = $max - $min;
        if ($range < 1) {
            return $min;
        } // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (1 << $bits) - 1; // set all lower bits to 1
        do {
            try {
                $pseudo = random_bytes($bytes);
            } catch (Exception $e) {
                throw new RuntimeException('IV generation failed');
            }
            $rnd = hexdec(bin2hex($pseudo));
            $rnd &= $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    /**
     * @param $length int Desired length of the token
     * @return string Returns the generated cryptographically-secure token
     */
    public static function Get($length): string
    {
        $token = '';
        $codeAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $codeAlphabet.= 'abcdefghijklmnopqrstuvwxyz';
        $codeAlphabet.= '0123456789';
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[self::crypto_rand_secure(0, $max-1)];
        }

        return $token;
    }

}
