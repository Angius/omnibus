<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 29.09.2018
 * Time: 04:05
 */

namespace Backend;

class Gravatar
{
    /**
     * @param $email
     * @param int $s
     * @param string $d
     * @param string $r
     * @param bool $img
     * @param array $attributes
     * @return string
     */
    public static function Get($email, $s = 80, $d = 'mp', $r = 'g', $img = false, array $attributes = array()): string
    {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($attributes as $key => $val) {
                $url .= ' ' . $key . '="' . $val . '"';
            }
            $url .= ' />';
        }
        return $url;
    }
}
