<?php

use Backend\Models\User;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 26.10.2018
 * Time: 05:30
 */

class Avatar
{
    public static function Upload(int $user, string $token, $file, Session $session, bool $is_patron = false): ?string
    {
        $_user = User::GetByID($user);

        $DIR = dirname(__DIR__).'/public/assets/img/avatars/';
        $ALLOWED_TYPES = array('image/jpeg', 'image/png');
        $ALLOWED_SIZE = 1024 * 500;

        // Patron privileges
        if ($is_patron) {
            $ALLOWED_TYPES[] = 'image/gif';
            $ALLOWED_SIZE *= 3;
        }

        // Auth user
        if (($session->get('token') !== $token) || ($_user->id !== $session->get('userid'))) {
            die ('UNAUTHORIZED ACCESS');
        }

        $file_name = $_user->id . '-' . $_user->name . '.' . explode('/', $file['type'])[1];
        $target_file = $DIR . $file_name;
        $uploadOk = 1;
        $imageFileType = $file['type'];

        // Check if image file is a actual image or fake image
        if (isset($_POST['submit'])) {
            $check = getimagesize($file['tmp_name']);
            if ($check !== false) {
                echo 'File is an image - ' . $check['mime'] . '.';
                $uploadOk = 1;
            } else {
                echo 'File is not an image.';
                $uploadOk = 0;
            }
        }
        // Check file size
        if ($file['size'] > $ALLOWED_SIZE) {
            echo 'Sorry, your file is too large.';
            $uploadOk = 0;
        }
        // Allow certain file formats
        if (!in_array($imageFileType, $ALLOWED_TYPES, false)) {
            echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk === 0) {
            return null;
        }

        // if everything is ok, try to upload file
        if (move_uploaded_file($file['tmp_name'], $target_file)) {
            return 'https://sfnw.online/public/assets/img/avatars/' . $file_name;
        }

        return 'Sorry, there was an error uploading your file.';
    }

}
