<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 04:29
 */

namespace Backend\Models;

use PDO;
use PDOException;

class Database {
    /**
     * Connects to database and returns a PDO database object
     * @return PDO
     * @throws PDOException
     */
    public static function Get(): PDO
    {
        $path = dirname(__DIR__, 2);
        $config = parse_ini_file($path.'/config.ini');

        $host = $config['host'];
        $name = $config['name'];
        $user = $config['user'];
        $pass = $config['pass'];

        // Try connecting to MySQL
        $dbh = new PDO("mysql:host=$host;dbname=$name;port=3306", $user, $pass);

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $dbh;
    }
}
