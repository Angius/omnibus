<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 31.07.2018
 * Time: 05:52
 */

namespace Backend\Models;

use PDO;
use PDOException;

class Role {
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $name;
    /**
     * @var bool
     */
    public $is_admin;
    /**
     * @var bool
     */
    public $is_author;
    /**
     * @var bool
     */
    public $matrix_access;
    /**
     * @var int
     */
    public $rank;

    /**
     * Role constructor.
     * @param int $id
     * @param string $name
     * @param bool $is_admin
     * @param bool $is_author
     * @param bool $matrix_access
     * @param int $rank
     */
    public function __construct(int $id, string $name, bool $is_admin, bool $is_author, bool $matrix_access, int $rank)
    {
        $this->id = $id;
        $this->name = $name;
        $this->is_admin = $is_admin;
        $this->is_author = $is_author;
        $this->matrix_access = $matrix_access;
        $this->rank = $rank;
    }

    /**
     * Gets all roles that exist in the database
     * @return array Returns an array of roles or a string with an error
     */
    public static function GetAll(): array
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM `roles`';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo 'Error!: [14]' . $e->getMessage() . '<br/>';
        }

        $roles = $sth->fetchAll();

        foreach ($roles as $key => $role) {
            $roles[$key] = self::Build($role);
        }
        return $roles;
    }

    /**
     * Gets all roles that have a lower or equal rank than provided
     * @param int $rank Desired rank
     * @return array|string Returns an array of roles or a string with an error
     */
    public static function GetLower(int $rank) {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM roles WHERE rank <= :rank';
        $sth = $dbh->prepare($sql);

        $sth->bindParam(':rank', $rank);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        $roles = $sth->fetchAll();

        foreach ($roles as $key => $role) {
            $roles[$key] = self::Build($role);
        }
        return $roles;
    }

    /**
     * Gets role by the specified ID number
     * @param int $id Takes an Integer that is the desired role ID
     * @return Role Returns a Role, or a String with an error
     */
    public static function GetByID(int $id): Role
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM `roles` WHERE `ID_role` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Role:1]'.$e->getMessage();
        }

        $role = $sth->fetch(PDO::FETCH_ASSOC);
        return self::Build($role);
    }

    /**
     * Transforms an assoc array returned from database query into a Role object
     * @param array $role Takes an assoc array describing a Role
     * @return Role Returns a built Role object
     */
    protected static function Build(array $role): Role
    {
        $r = new Role(
            $role['ID_role'],
            $role['name_role'],
            $role['is_admin'],
            $role['is_author'],
            $role['matrix_access'],
            $role['rank']
        );
        return $r;
    }

}
