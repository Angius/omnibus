<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 16:45
 */

namespace Backend\Models;

use Backend\Gravatar;
use DateTime;
use Exception;
use Fuse\Fuse;
use PDO;
use PDOException;
use RuntimeException;

/**
 * Class User
 * @property int $id
 * @property string $name
 * @property string $bio
 * @property string $title
 * @property string $link
 * @property string $avatar
 * @property string $email
 * @property datetime $joined;
 * @property datetime $last_seen
 * @property Comment $comment
 * @property string $password
 * @property string $code
 * @property bool $activated
 * @property bool $muted
 * @property Role $role
 */
class User
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $bio;
    /** @var string */
    private $title;
    /** @var string */
    private $link;
    /** @var string */
    private $avatar;
    /** @var string */
    private $email;
    /** @var datetime */
    private $joined;
    /** @var datetime */
    private $last_seen;
    /** @var int */
    private $comments;
    /** @var string */
    private $password;
    /** @var string */
    private $code;
    /** @var bool */
    private $activated;
    /** @var bool */
    private $muted;
    /** @var Role */
    private $role;


    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $bio
     * @return User
     */
    public function setBio(?string $bio): User
    {
        $this->bio = $bio;
        return $this;
    }

    /**
     * @param string $title
     * @return User
     */
    public function setTitle(?string $title): User
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $link
     * @return User
     */
    public function setLink(?string $link): User
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @param string $avatar
     * @return User
     */
    public function setAvatar(string $avatar): User
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param string $joined
     * @return User
     * @throws Exception
     */
    public function setJoined(string $joined): User
    {
        $this->joined = new DateTime($joined);
        return $this;
    }

    /**
     * @param string $last_seen
     * @return User
     * @throws Exception
     */
    public function setLastSeen(string $last_seen): User
    {
        $this->last_seen = new DateTime($last_seen);
        return $this;
    }

    /**
     * @param int $comments
     * @return User
     */
    public function setComments(?int $comments): User
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @param string $password
     * @param bool $should_be_hashed
     * @return User
     */
    public function setPassword(string $password, bool $should_be_hashed = false): User
    {
        $this->password = $should_be_hashed ? password_hash($password, PASSWORD_BCRYPT) : $password;
        return $this;
    }

    /**
     * @param string $code
     * @return User
     */
    public function setCode(?string $code): User
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @param bool $activated
     * @return User
     */
    public function setActivated(bool $activated): User
    {
        $this->activated = $activated;
        return $this;
    }

    /**
     * @param bool $muted
     * @return User
     */
    public function setMuted(?bool $muted): User
    {
        $this->muted = $muted;
        return $this;
    }

    /**
     * @param int $role
     * @return User
     */
    public function setRole(int $role): User
    {
        $this->role = Role::GetByID($role);
        return $this;
    }


    public function &__get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        throw new RuntimeException('Setting properties without proper setters is not allowed');
    }

    public function __isset($name)
    {
        return isset($this->$name);
    }


    /**
     * Gets all users that exist in the database
     * @return array Returns an array of users or a string with an error
     * @throws Exception
     */
    public static function GetAll(): array
    {
        $dbh = Database::Get();

        // Fetch all users
        $sql = 'SELECT * FROM users ORDER BY user_ID_role, name_user';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[1]'.$e->getMessage();
        }

        $users = $sth->fetchAll();

        foreach ($users as $key => $user) {
            $users[$key] = $user !== null ? self::Build($user) : null;
        }
        return $users;
    }

    /**
     * Gets all users that possess one of the specified roles
     * @param bool $user Should return users with User role?
     * @param bool $author Should return users with Author role?
     * @param bool $admin Should return users with Admin role?
     * @return array Returns an array of User objects or a string with an error
     * @throws Exception
     */
    public static function GetAllByRole(bool $user = false, bool $author = false, bool $admin = false): array
    {
        $dbh = Database::Get();

        $roles = [];
        $roles[] = $user ? 'user_ID_role = 3' : '';
        $roles[] = $author ? 'user_ID_role = 2' : '';
        $roles[] = $admin ? 'user_ID_role = 1' : '';
        $roles = implode(' OR ', array_filter($roles, '\strlen'));

        // Fetch all users
        $sql = 'SELECT * FROM users WHERE ' . $roles . '
                ORDER BY user_ID_role, name_user';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[2]'.$e->getMessage();
        }

        $users = $sth->fetchAll();

        foreach ($users as $key => $u) {
            $users[$key] = $u !== null ? self::Build($u) : null;
        }
        return $users;
    }

    /**
     * Gets user by the specified ID number
     * @param int $id Takes an Integer that is the desired user ID
     * @return User Returns a User, or a String with an error
     * @throws Exception
     */
    public static function GetByID(int $id): User
    {
        return self::Get('ID_user', $id);
    }

    /**
     * Gets user by name
     * @param string $name Takes a string that is the user name
     * @return User Returns a User, or a String with an error
     * @throws Exception
     */
    public static function GetByName(string $name): User
    {
        return self::Get('name_user', $name);
    }

    /**
     * Gets user by email
     * @param string $email Takes a string that is the user email
     * @return User Returns a User, or a String with an error
     * @throws Exception
     */
    public static function GetByEmail(string $email): User
    {
        return self::Get('email_user', $email);
    }

    /**
     * Checks if a user with the token exists
     * @param string $token Takes a token to check against
     * @return null|User Returns the found user or null if no such user exists
     * @throws Exception
     */
    public static function GetByToken(string $token): ?User
    {
        return self::Get('code_user', $token);
    }

    /**
     * @param string $by
     * @param $param
     * @return User|null
     * @throws PDOException
     * @throws Exception
     */
    private static function Get(string $by, $param): ?User
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM users';
        $sql .= " WHERE $by = :id";

        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $param);

        $sth->execute();

        $user = $sth->fetch(PDO::FETCH_ASSOC);
        return ($user === null || $user === false) ? null : self::Build($user);
    }

    /**
     * Gets users with titles that match the query
     * @param string $query Takes a query to match the results against
     * @param int $limit Takes a number of results to return
     * @return array Returns an array of results, ordered by how well they match
     * @throws Exception
     */
    public static function Search(string $query, int $limit = 5): array
    {
        $users = json_decode(json_encode(self::GetAll()), true);

        $fuse = new Fuse($users,
            [
                'keys' => ['name'],
                'includeScore' => true,
            ]
        );

        $r = $fuse->search($query);
        $rl = $limit > 0 ? array_chunk($r, $limit)[0] : $r;

        $out = [];
        foreach ($rl as $user) {
            $out[] = array(
                'id' => $user['item']['id'],
                'name' => $user['item']['name'],
                'bio' => $user['item']['bio'],
                'title' => $user['item']['title'],
                'link' => $user['item']['link'],
                'avatar' => $user['item']['avatar'],
                'joined' => $user['item']['joined'],
                'last seen' => $user['item']['last seen'],
                'role' => $user['item']['role']['name'],
                'similarity' => $user['score']
            );
        }

        return $out;
    }

    /**
     * @return void
     */
    public function Add(): void
    {
        $dbh = Database::Get();
        $sql = 'INSERT INTO users (name_user, password, email_user, user_ID_role, code_user, avatar_user) 
                             VALUES (:name, :password, :email, :role, :code, :avatar)';
        $sth = $dbh->prepare($sql);

        $avatar = Gravatar::Get($this->email);
        $code = $this->code ?: '';

        $sth->bindParam(':name', $this->name);
        $sth->bindParam(':password', $this->password);
        $sth->bindParam(':email', $this->email);

        $sth->bindValue(':role', 3, PDO::PARAM_INT);
        $sth->bindParam(':code', $code);
        $sth->bindParam(':avatar', $avatar);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[3]'.$e->getMessage();
        }
    }

    /**
     * @return void
     */
    public function Edit(): void
    {
        $dbh = Database::Get();

        // Modify user
        $sth = $dbh->prepare('UPDATE users 
                                        SET name_user = :name, 
                                            bio_user = :bio,
                                            title_user = :title,
                                            link_user = :link,
                                            avatar_user = :avatar,
                                            email_user = :email,
                                            password = :password,  
                                            user_ID_role = :role
                                        WHERE ID_user = :id');

        // Check if variables are set
        $sth->bindParam(':name', $this->name);
        $sth->bindParam(':bio', $this->bio);
        $sth->bindParam(':title', $this->title);
        $sth->bindParam(':link', $this->link);
        $sth->bindParam(':avatar', $this->avatar);
        $sth->bindParam(':email', $this->email);
        $sth->bindParam(':password', $this->password);
        $sth->bindParam(':role', $this->role->id);
        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[4]'.$e->getMessage();
        }
    }

    /**
     * Activates the user's account
     */
    public function Activate(): void
    {
        $this->setActivated(true);
        $this->Edit();
    }

    /**
     * Updates the LastSeen date of the user
     * @return void
     * @throws Exception
     */
    public function UpdateLastSeen(): void
    {
        $date = date('d-m-Y H:i:s');
        $this->setLastSeen($date);
        $this->Edit();
    }

    /**
     * Deletes the user from database
     * @return void
     */
    public function Delete(): void
    {
        $dbh = Database::Get();
        // Change all articles' author to [DELETED]
        $sql = 'UPDATE articles
                SET article_ID_user = 25
                WHERE article_ID_user = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $this->id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[5]'.$e->getMessage();
        }

        // Change all comments' author to [DELETED]
        $sql = 'UPDATE comments
                SET comment_ID_user = 25
                WHERE comment_ID_user = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $this->id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[6]'.$e->getMessage();
        }

        // Delete user
        $sql = 'DELETE FROM users WHERE ID_user = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $this->id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[7]'.$e->getMessage();
        }
    }

    /**
     * Counts users currently in database
     * @return int Returns an Integer equal to the amount of users, or -1 in case of an error
     */
    public static function Count(): int
    {
        $dbh = Database::Get();
        $sql = 'SELECT COUNT(*)
            FROM users
            WHERE activated_user = TRUE';

        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[8]'.$e->getMessage();
            return -1;
        }

        return (int)$sth->fetchColumn();
    }

    /**
     * @param int $id
     * @param string $token
     * @return int|string
     */
    public static function SetRememberMe(int $id, string $token)
    {
        $dbh = Database::Get();

        // Modify user
        $sql = 'UPDATE users 
                SET remember_me = :token
                WHERE ID_user = :id';

        $sth = $dbh->prepare($sql);

        $new = null;
        try {
            $sth->execute(
                array(
                    ':token' => $token,
                    ':id' => $id,
                )
            );
        } catch (PDOException $e) {
            echo '[9]'.$e->getMessage();
        }

        return (int)$sth->rowCount();
    }

    /**
     * @param int $id
     * @return mixed|null|string
     */
    public static function GetRememberMe(int $id)
    {
        $dbh = Database::Get();

        $sql = 'SELECT remember_me 
                FROM users 
                WHERE ID_user = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[10]'.$e->getMessage();
        }

        $user = $sth->fetchColumn();
        return ($user === null || $user === false) ? null : $user;
    }

    /**
     * Transforms an assoc array returned from database query into a User object
     * @param array $user Takes an assoc array describing a User
     * @return User returns a built User object
     * @throws Exception
     */
    protected static function Build(array $user): User
    {
        $u = new self();
        $u->setId($user['ID_user'])
          ->setName($user['name_user'])
          ->setBio($user['bio_user'])
          ->setTitle($user['title_user'])
          ->setLink($user['link_user'])
          ->setAvatar($user['avatar_user'])
          ->setEmail($user['email_user'])
          ->setJoined($user['joined_user'])
          ->setLastSeen($user['last_seen_user'])
          ->setComments($user['thread_ID_user'])
          ->setPassword($user['password'])
          ->setCode($user['code_user'])
          ->setActivated($user['activated_user'])
          ->setMuted($user['muted_user'])
          ->setRole($user['user_ID_role']);
        return $u;
    }
}
