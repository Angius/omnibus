<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 01.08.2018
 * Time: 03:28
 */

namespace Backend\Models;

use PDO;
use PDOException;

class Comment
{
    /** @var */
    public $id;
    /** @var */
    public $thread;
    /** @var */
    public $author;
    /** @var */
    public $body;
    /** @var */
    public $date;
    /** @var */
    public $reported_by;
    /** @var */
    public $report_date;
    /** @var */
    public $author_role;
    /** @var */
    public $author_avatar;
    /** @var */
    public $reporter;
    /** @var */
    public $reporter_role;

    /**
     * Comment constructor.
     * @param $id
     * @param $thread
     * @param $author
     * @param $body
     * @param $date
     * @param $reported_by
     * @param $report_date
     * @param $author_role
     * @param $author_avatar
     * @param $reporter
     * @param $reporter_role
     */
    public function __construct($id, $thread, $author, $body, $date, $reported_by, $report_date, $author_role, $author_avatar, $reporter, $reporter_role)
    {
        $this->id = $id;
        $this->thread = $thread;
        $this->author = $author;
        $this->body = $body;
        $this->date = $date;
        $this->reported_by = $reported_by;
        $this->report_date = $report_date;
        $this->author_role = $author_role;
        $this->author_avatar = $author_avatar;
        $this->reporter = $reporter;
        $this->reporter_role = $reporter_role;
    }

    /**
     * @param $body
     * @param $author
     * @param $thread
     * @return string|null
     */
    public static function Add($body, $author, $thread): ?string
    {
        $dbh = Database::Get();

        $length = strlen($body);
        if ($length <= 0 || $length > 10000) {
            return "Message too long: {$length}/10000";
        }

        $sql = 'INSERT INTO `comments` 
                (comment_ID_thread, comment_ID_user, body_comment, date_comment) 
                VALUES (:thread, :user, :comment, :date)';
        $sth = $dbh->prepare($sql);

        $sth->bindParam('thread', $thread, PDO::PARAM_INT);
        $sth->bindParam('user', $author, PDO::PARAM_INT);
        $sth->bindParam('comment', $body);
        $sth->bindValue('date', date('Y-m-d H:i:s'));

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        return null;
    }

    /**
     * @param int $id
     * @param int $user
     * @return string|null
     */
    public static function Report(int $id, int $user): ?string
    {
        $dbh = Database::Get();

        $sql = 'UPDATE `comments` 
                SET `reported_by_comment` = :user,
                    `report_date_comment` = :date
                WHERE `ID_comment` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':user', $user, PDO::PARAM_INT);
        $sth->bindValue(':date', date('Y-m-d h:i:s'));
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        return null;
    }

    /**
     * @param int $id
     * @return string|null
     */
    public static function Approve(int $id): ?string
    {
        $dbh = Database::Get();

        $sql = 'UPDATE `comments` 
                SET `reported_by_comment` = NULL,
                    `report_date_comment` = NULL
                WHERE `ID_comment` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        return null;
    }

    /**
     * @param int $id
     * @return string|null
     */
    public static function Delete(int $id): ?string
    {
        $dbh = Database::Get();

        $sql = 'DELETE FROM `comments` WHERE `ID_comment` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        return null;
    }

    /**
     * @param bool $ascending
     * @param int|null $thread
     * @return array|string
     */
    public static function GetAll(bool $ascending = false, int $thread = null)
    {
        $dbh = Database::Get();

        // Fetch all tags
        $sql = 'SELECT comments.*,
                       users.name_user,
                       users.user_ID_role,
                       users.avatar_user,
                       roles.name_role,
                       user_report
                FROM comments
                       LEFT JOIN reports r on comments.ID_comment = r.comment_report
                       JOIN users ON comments.comment_ID_user = users.ID_user
                       JOIN roles ON users.user_ID_role = roles.ID_role';

        $sql .= $thread !== null ? ' WHERE comments.comment_ID_thread = :id' : '';
        $sql .= ' ORDER BY comments.date_comment';
        $sql .= $ascending ? ' ASC' : ' DESC';

        $sth = $dbh->prepare($sql);
        if ($thread !== null) {
            $sth->bindParam(':id', $thread, PDO::PARAM_INT);
        }

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [14]' . $e->getMessage() . '<br/>';
        }

        $comments = $sth->fetchAll();

        foreach ($comments as $key => $com) {
            $comments[$key] = self::Build($com);
        }
        return $comments;
    }

    /**
     * @param array $comment
     * @return Comment
     */
    protected static function Build(array $comment): Comment
    {
        return new self(
            $comment['ID_comment'],
            $comment['comment_ID_thread'],
            $comment['name_user'],
            $comment['body_comment'],
            $comment['date_comment'],
            $comment['reported_by_comment'],
            $comment['report_date_comment'],
            $comment['name_role'],
            $comment['avatar_user'],
            $comment['user_report'],
            $comment['reporter_role'] ?? ''
        );
    }

    /**
     * @param int $user
     * @return array|string
     */
    public static function GetByUser(int $user)
    {
        $dbh = Database::Get();

        // Fetch all tags
        $sql = 'SELECT * FROM `comments` WHERE `comment_ID_user` = :user';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute(
                array(
                    ':user' => $user,
                )
            );
        } catch (PDOException $e) {
            return 'Error!: [14]' . $e->getMessage() . '<br/>';
        }

        $comments = $sth->fetchAll();

        foreach ($comments as $key => $com) {
            $comments[$key] = self::Build($com);
        }
        return $comments;
    }

    /**
     * @return array|string
     */
    public static function GetReported()
    {
        $dbh = Database::Get();

        // Fetch all tags
        $sql = 'SELECT comments.*,
                       users.name_user,
                       reporters.name_user   AS user_report,
                       users.ID_user,
                       reporters.ID_user     AS ID_reporter,
                       users.avatar_user,
                       reporters.avatar_user AS avatar_reporter,
                       roles.name_role       AS user_role,
                       roles_r.name_role     AS reporter_role
                FROM comments
                   JOIN users
                        ON comments.comment_ID_user = users.ID_user
                   JOIN users reporters
                        ON comments.reported_by_comment = reporters.ID_user
                   JOIN roles
                        ON users.user_ID_role = roles.ID_role
                   JOIN roles roles_r
                        ON reporters.user_ID_role = roles_r.ID_role
                ORDER BY comments.report_date_comment DESC';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [15]' . $e->getMessage() . '<br/>';
        }

        $comments = $sth->fetchAll();

        foreach ($comments as $key => $com) {
            $comments[$key] = self::Build($com);
        }
        return $comments;
    }

    /**
     * @param null $id
     * @return int
     */
    public static function Count($id = null): int
    {
        $dbh = Database::Get();

        if ($id !== null) {
            $sql = 'SELECT COUNT(*) FROM comments';
            $sth = $dbh->prepare($sql);
        } else {
            $sql = 'SELECT COUNT(*) FROM comments WHERE comment_ID_user = :id';
            $sth = $dbh->prepare($sql);
            $sth->bindParam(':id', $id);
        }

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        return $sth->fetchColumn();
    }
}
