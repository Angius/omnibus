<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 15.08.2018
 * Time: 05:45
 */

namespace Backend\Models;

use Exception;

class Quote {
    public $quote;
    public $author;

    /**
     * Quote constructor.
     * @param $quote
     * @param $author
     */
    public function __construct($quote, $author)
    {
        $this->quote = $quote;
        $this->author = $author;
    }

    /**
     * Function that grabs all quotes from the json
     * @return array
     */
    public static function GetQuotes(): array
    {
        $json = file_get_contents(dirname(__DIR__,2). '/public/assets/quotes.json');
        return json_decode($json, true);
    }

    /**
     * @return Quote
     */
    public static function GetRandomQuote(): Quote
    {
        $quotes = self::GetQuotes();
        try {
            $rand = random_int(0, count($quotes));
            $quote = $quotes[$rand];
            return new Quote($quote['Quote'], $quote['Author']);
        } catch (Exception $e) {
            return new Quote('Error fetching quotes', 'Debugger');
        }
    }
}
