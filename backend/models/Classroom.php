<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 01.08.2018
 * Time: 01:04
 */

namespace Backend\Models;

use PDO;
use PDOException;

class Classroom
{
    public $id;
    public $start_date;
    public $end_date;
    public $start_title;
    public $end_title;
    public $start_body;
    public $end_body;

    /**
     * Classroom constructor.
     * @param $id
     * @param $start_date
     * @param $end_date
     * @param $start_title
     * @param $end_title
     * @param $start_body
     * @param $end_body
     */
    public function __construct($id, $start_date, $end_date, $start_title, $end_title, $start_body, $end_body)
    {
        $this->id = $id;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->start_title = $start_title;
        $this->end_title = $end_title;
        $this->start_body = $start_body;
        $this->end_body = $end_body;
    }

    /**
     * Gets all classrooms that exist in the database
     * @return array|string Returns an array of classrooms or a string with an error
     */
    public static function GetAllDates()
    {
        $dbh = Database::Get();
        $sql = 'SELECT `ID_class`, `date_start_class` FROM `classes` ORDER BY `ID_class` DESC';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [2]' . $e->getMessage() . '<br/>';
        }

        $dates = $sth->fetchAll();

        foreach ($dates as $key => $date) {
            $dates[$key] = array(
                'id' => $date['ID_class'],
                'date' => $date['date_start_class']
            );
        }

        return $dates;
    }

    /**
     * Gets all classrooms that exist in the database
     * @return array|string Returns an array of classrooms or a string with an error
     */
    public static function GetAll()
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM `classes` ORDER BY `ID_class` DESC';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [2]' . $e->getMessage() . '<br/>';
        }

        $classes = $sth->fetchAll();

        foreach ($classes as $key => $class) {
            $classes[$key] = self::Build($class);
        }

        return $classes;
    }

    /**
     * Transforms an assoc array returned from database query into a Classroom object
     * @param array $class Takes an assoc array describing a Classroom
     * @return Classroom returns a built Classroom object
     */
    protected static function Build(array $class): Classroom
    {
        return new Classroom(
            $class['ID_class'],
            $class['date_start_class'],
            $class['date_end_class'],
            $class['title_start_class'],
            $class['title_end_class'],
            $class['body_start_class'],
            $class['body_end_class']
        );
    }

    /**
     * Gets classroom by the specified ID number
     * @param int $id Takes an Integer that is the desired classroom ID
     * @return Classroom|string Returns a Classroom, or a String with an error
     */
    public static function GetByID(int $id = 1)
    {
        $dbh = Database::Get();
        $sql = 'SELECT * FROM `classes` WHERE `ID_class` = :id LIMIT 1';
        $sth = $dbh->prepare($sql);
        $sth->bindValue(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        $class = $sth->fetch(PDO::FETCH_ASSOC);

        return self::Build($class);
    }

    /**
     * Gets latest classroom
     * @return Classroom|string Returns a Classroom, or a String with an error
     */
    public static function GetLatest()
    {
        $dbh = Database::Get();
        $sql = 'SELECT * FROM `classes` ORDER BY `ID_class` DESC LIMIT 1';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        $class = $sth->fetch(PDO::FETCH_ASSOC);

        return self::Build($class);
    }

    /**
     * Deletes a class from the database
     * @param $id int ID of the class to be deleted
     * @return null|string Returns null if successful or an error string
     */
    public static function Delete(int $id): ?string
    {
        $dbh = Database::Get();
        // Delete class
        $sql = 'DELETE FROM `classes` WHERE `ID_class` = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        return null;
    }

    /** Adds Class to database
     * @param string $start_date
     * @param string $end_date
     * @param string $start_title
     * @param string $end_title
     * @param string $start_body
     * @param string $end_body
     * @return null|string
     */
    public static function Add(string $start_date, string $end_date, string $start_title, string $end_title, string $start_body, string $end_body): ?string
    {
        $dbh = Database::Get();
        // Add class to database
        $sql = 'INSERT INTO `classes` (date_start_class, date_end_class, title_start_class, title_end_class, body_start_class, body_end_class)
                VALUES (:sdate, :edate, :stitle, :etitle, :sbody, :ebody)';

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':sdate', $start_date);
        $sth->bindParam(':edate', $end_date);
        $sth->bindParam(':stitle', $start_title);
        $sth->bindParam(':etitle', $end_title);
        $sth->bindParam(':sbody', $start_body);
        $sth->bindParam(':ebody', $end_body);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * Edits class
     * @param int $id
     * @param string $start_date
     * @param string $end_date
     * @param string $start_title
     * @param string $end_title
     * @param string $start_body
     * @param string $end_body
     * @return null|string
     */
    public static function Edit(int $id, string $start_date, string $end_date, string $start_title, string $end_title, string $start_body, string $end_body): ?string
    {
        $dbh = Database::Get();
        // If everything's fine, add this shit to the database
        $sth = $dbh->prepare('  UPDATE `classes` 
                                                SET `title_start_class` = :stitle,
                                                    `title_end_class` = :etitle,
                                                    `body_start_class` = :sbody,
                                                    `body_end_class` = :ebody,
                                                    `date_start_class` = :sdate,
                                                    `date_end_class` = :edate
                                                WHERE `ID_class` = :id');

        $sth->bindParam(':sdate', $start_date);
        $sth->bindParam(':edate', $end_date);
        $sth->bindParam(':stitle', $start_title);
        $sth->bindParam(':etitle', $end_title);
        $sth->bindParam(':sbody', $start_body);
        $sth->bindParam(':ebody', $end_body);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        return null;
    }

    /**
     * Counts classrooms currently in database
     * @return int|string Returns an Integer equal to the amount of classrooms, or a String with an error
     */
    public static function Count()
    {
        $dbh = Database::Get();
        $sql = 'SELECT COUNT(*) FROM `classes`';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            return 'Error!: [6]' . $e->getMessage() . '<br/>';
        }

        return (int)$sth->fetchColumn();
    }
}
