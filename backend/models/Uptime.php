<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 28.09.2018
 * Time: 19:54
 */

namespace Backend\Models;

use Backend\Settings;

class Uptime
{
    private static $_statuses = array(
        0 => array(
            'icon' => 'gray pause',
            'type' => 'paused'
        ),
        1 => array(
            'icon' => 'gray clock outline',
            'type' => 'not checked yet'
        ),
        2 => array(
            'icon' => 'green check',
            'type' => 'up'
        ),
        8 => array(
            'icon' => 'yellow question circle outline',
            'type' => 'seems down'
        ),
        9 => array(
            'icon' => 'red close',
            'type' => 'down'
        ),
    );

    private static $_log_statuses = array(
        1 => array(
            'icon' => 'red close',
            'type' => 'down'
        ),
        2 => array(
            'icon' => 'green check',
            'type' => 'up'
        ),
        98 => array(
            'icon' => 'gray pause',
            'type' => 'paused'
        ),
        99 => array(
            'icon' => 'blue play',
            'type' => 'started'
        ),
    );

    public static function GetStatus(string $type)
    {
        return self::$_statuses[self::Get($type)->monitors[0]->status];
    }

    public static function Get(string $type)
    {
        $api_keys = array(
            'discord-bot' => Settings::Read('discord-bot'),
            'omnibus'     => Settings::Read('omnibus'),
        );

        // Access API via cURL:
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.uptimerobot.com/v2/getMonitors',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'api_key=' . $api_keys[$type] . '&format=json&logs=1&log_types=1&logs_limit=1&all_time_uptime_ratio=1',
            CURLOPT_HTTPHEADER => array(
                'cache-control: no-cache',
                'content-type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo 'cURL Error #:' . $err;
        }

        // Decode JSON response and get only the data needed:
        $response = json_decode($response, false);

        return $response;
    }

    public static function GetUptime(string $type): int
    {
        return self::Get($type)->monitors[0]->all_time_uptime_ratio;
    }

    public static function GetDowntime(string $type): int
    {
        return 100 - self::Get($type)->monitors[0]->all_time_uptime_ratio;
    }

    public static function GetLogs(string $type): array
    {
        $resp = self::Get($type)->monitors[0]->logs;
        $out = array();

        foreach ($resp as $r) {
            $out[] = array(
                'status'   => self::$_log_statuses[$r->type],
                'time'     => date('d.m.Y, H:i:s', $r->datetime),
                'duration' => self::format_time($r->duration),
                'reason'   => $r->reason->detail,
            );
        }

        return $out;
    }

    private static function format_time($time, $separator=':'): string
    {
        return sprintf('%02d%s%02d%s%02d', floor($time/3600), $separator, ($time/60)%60, $separator, $time%60);
    }
}

