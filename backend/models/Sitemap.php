<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 17.08.2018
 * Time: 05:41
 */

namespace Backend\Models;

use Backend\Token;

class Sitemap {

    /**
     * Sends a request to Codepunker API asking for sitemap generation
     * @param string $token Takes a Codepunker authorization token from Sitemap::Authorize()
     * @param string $callback Callback URL for the API
     * @return string Returns a message regarding sitemap generation
     */
    public static function GetSitemap(string $token, string $callback): string
    {
        $url = 'https://www.codepunker.com/tools';
        $data = array(
            'execute' => 'executeSitemapGenerator',
            'domain' => 'https://sfnw.online',
            'freq' => 'daily',
            'token' => $token,
            'callbackuri' => $callback
        );

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        if ($result === FALSE) { return null; }
        return $result;
    }

    /**
     * Sends a request to Codepunker API asking for an authorization token
     * @param string $key Takes a Codepunker API key
     * @return string Returns an authorization token or null if authorization failed
     */
    public static function Authorize(string $key): string
    {
        $url = 'https://www.codepunker.com/tools';
        $data = array(
            'execute' => 'authorizeAPI',
            'key' => $key,
            'rand' => Token::Get(32)
        );

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        if ($result === FALSE) { return null; }
        return $result;
    }

}
