<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 28.09.2018
 * Time: 03:24
 */

namespace Backend\Models;

class Discord
{
    public static function Online(): int
    {
        $jsonIn = file_get_contents('https://discordapp.com/api/guilds/207507139130949632/embed.json');
        $JSON = json_decode($jsonIn, true);

        return count($JSON['members']);
    }
}
