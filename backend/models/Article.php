<?php
/**
 * Contains the Article object definition and methods
 *
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 04:20
 */

namespace Backend\Models;

use Exception;
use Fuse\Fuse;
use PDO;
use PDOException;
use RuntimeException;

/**
 * Class Article
 * @package Backend\Models
 * @property int $id
 * @property string $title
 * @property string $furl
 * @property string $body
 * @property string $date
 * @property string $excerpt
 * @property Category $category
 * @property array $tags
 * @property User $author
 * @property int $comments
 */
class Article
{
    /* @var int */
    private $id;
    /* @var string */
    private $title;
    /* @var string */
    private $furl;
    /* @var string */
    private $body;
    /* @var string */
    private $date;
    /* @var string */
    private $excerpt;
    /* @var Category */
    private $category;
    /* @var array */
    private $tags;
    /* @var User */
    private $author;
    /* @var int */
    private $comments;

    /**
     * @param int $id
     * @return Article
     */
    public function setId(int $id): Article
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $title
     * @return Article
     */
    public function setTitle(string $title): Article
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $furl
     * @return Article
     */
    public function setFurl(string $furl): Article
    {
        $this->furl = $furl;
        return $this;
    }

    /**
     * @param string $body
     * @return Article
     */
    public function setBody(string $body): Article
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param string $date
     * @return Article
     */
    public function setDate(string $date): Article
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @param string $excerpt
     * @return Article
     */
    public function setExcerpt(string $excerpt): Article
    {
        $this->excerpt = $excerpt;
        return $this;
    }

    /**
     * @param int $category
     * @return Article
     */
    public function setCategory(int $category): Article
    {
        $this->category = Category::GetByID($category);
        return $this;
    }

    /**
     * @param array $tags
     * @return Article
     * @throws Exception
     */
    public function setTags(array $tags): Article
    {
        $this->tags = [];
        foreach ($tags as $t) {
            $this->tags[] = Tag::GetByID($t);
        }
        return $this;
    }

    /**
     * @param int $article
     * @return Article
     */
    public function setTagsByArticle(int $article): Article
    {
        $this->tags = Tag::GetByArticle($article);
        return $this;
    }

    /**
     * @param int $author
     * @return Article
     * @throws Exception
     */
    public function setAuthor(int $author): Article
    {
        $this->author = User::GetByID($author);
        return $this;
    }

    /**
     * @param int $comments
     * @return Article
     */
    public function setComments(int $comments): Article
    {
        $this->comments = $comments;
        return $this;
    }


    public function &__get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        throw new RuntimeException('Setting properties without proper setters is not allowed');
    }

    public function __isset($name)
    {
        return isset($this->$name);
    }


    /**
     * Gets articles with titles that match the query
     * @param string $query Takes a query to match the results against
     * @param int $limit Takes a number of results to return
     * @return array Returns an array of results, ordered by how well they match
     * @throws Exception
     */
    public static function Search(string $query, int $limit = 5): array
    {
        $arts = json_decode(json_encode(self::GetAll()), true);

        $fuse = new Fuse($arts,
            [
                'keys' => ['title', 'author'],
                'includeScore' => true,
            ]
        );

        $r = $fuse->search($query);

        if (!empty($r)) {
            $rl = $limit > 0 ? array_chunk($r, $limit)[0] : $r;

            $out = [];
            foreach ($rl as $art) {
                $out[] = [
                    'id' => $art['item']['id'],
                    'title' => $art['item']['title'],
                    'furl' => $art['item']['furl'],
                    'body' => $art['item']['body'],
                    'date' => $art['item']['date'],
                    'excerpt' => $art['item']['excerpt'],
                    'category' => $art['item']['category'],
                    'tags' => $art['item']['tags'],
                    'author' => [
                        'id' => $art['item']['author']['id'],
                        'name' => $art['item']['author']['name'],
                        'title' => $art['item']['author']['title'],
                        'avatar' => $art['item']['author']['avatar'],
                        'role' => $art['item']['author']['role']['name'],
                    ],
                    'similarity' => $art['score']
                ];
            }
            return $out;
        }

        return [];
    }

    /**
     * Gets an article by a specified parameter
     * @param string|null $by Takes a Source of the articles
     * @param int|null $id Takes an ID of the selected source
     * @param int $limit Takes a limit of how many results should be returned
     * @param int $offset
     * @return array Returns an array of articles or an error string
     * @throws Exception
     */
    private static function Get(?string $by = null, ?int $id = 1, int $limit = 0, int $offset = 0): array
    {
        $dbh = Database::Get();
        $sql = 'SELECT articles.*, categories.*, tags.*, article_has_tags.*, users.*
                FROM articles
                    JOIN categories ON articles.article_ID_category = categories.ID_category
                    JOIN users ON articles.article_ID_user = users.ID_user
                    JOIN article_has_tags ON article_has_tags.tags_ID_article = articles.ID_article
                    JOIN tags ON article_has_tags.tags_ID_tag = tags.ID_tag';

        $sql .= $by ? " WHERE $by = :id" : '';
        $sql .= ' ORDER BY date_article DESC';

        // Add limit if necessary
        $sql = $limit > 0 ? $sql . ' LIMIT :l' : $sql;
        $li = (int)trim($limit);

        // Add offset if necessary
        $sql = $offset > 0 ? $sql . ' OFFSET :o' : $sql;
        $of = (int)trim($offset);

        $sth = $dbh->prepare($sql);

        if ($limit > 0) {
            $sth->bindParam('l', $li, PDO::PARAM_INT);
        }
        if ($offset > 0) {
            $sth->bindParam('o', $of, PDO::PARAM_INT);
        }
        if ($by !== null) {
            $sth->bindParam('id', $id, PDO::PARAM_INT);
        }

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:1]'.$e->getMessage();
        }

        $articles = $sth->fetchAll();

        foreach ($articles as $key => $art) {
            $articles[$key] = self::Build($art);
        }

        return $articles;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws Exception
     */
    public static function GetAll(int $limit = 0, int $offset = 0): array
    {
        return self::Get(null, null, $limit, $offset);
    }


    /**
     * @param int $id
     * @return Article
     * @throws Exception
     */
    public static function GetByID(int $id): Article
    {
    // throw new Exception($id);
        return self::Get('ID_article', $id, 1)[0];
    }

    /**
     * @param int $id
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws Exception
     */
    public static function GetByCategory(int $id = 1, int $limit = 0, int $offset = 0): array
    {
        return self::Get('ID_category', $id, $limit, $offset);
    }

    /**
     * @param int $id
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws Exception
     */
    public static function GetByAuthor(int $id = 1, int $limit = 0, int $offset = 0): array
    {
        return self::Get('article_ID_user', $id, $limit, $offset);
    }

    /**
     * @param int $id
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws Exception
     */
    public static function GetByTag(int $id = 1, int $limit = 0, int $offset = 0): array
    {
        return self::Get('ID_tag', $id, $limit, $offset);
    }

    /**
     * Counts articles currently in the database
     * @return int Returns an Integer equal to the amount of articles, or a String with an error
     */
    public static function Count(): int
    {
        $dbh = Database::Get();
        $sql = 'SELECT COUNT(*) FROM articles';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:2]'.$e->getMessage();
        }

        return (int)$sth->fetchColumn();
    }

    /**
     * @return int
     */
    public function Add(): int
    {
        $dbh = Database::Get();

        // Create a comment thread
        $sql = "INSERT INTO comment_threads (ID_thread, source_thread) VALUES (NULL, 'article')";
        $sth = $dbh->prepare($sql);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:3]'.$e->getMessage();
        }

        $thread_id = $dbh->lastInsertId();

        // If all's cool, add the art to the db
        $sql = 'INSERT INTO articles (title_article, friendly_title_article, body_article, date_article, excerpt_article, article_ID_category, article_ID_user, article_ID_thread)
                            VALUES (:title, :friendly, :body, :date, :excerpt, :category, :author, :thread)';

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':title', $this->title);
        $sth->bindValue(':friendly', self::friendlify($this->title));
        $sth->bindParam(':body', $this->body);
        $sth->bindParam(':date', $this->date);
        $sth->bindParam(':excerpt', $this->excerpt);
        $sth->bindParam(':category', $this->category->id);
        $sth->bindParam(':author', $this->author->id);
        $sth->bindParam(':thread', $thread_id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:4]'.$e->getMessage();
        }

        $lastid = $dbh->lastInsertId();

        // Add tags too!
        foreach ($this->tags as $tag) {
            $sth = $dbh->prepare('INSERT INTO article_has_tags (tags_ID_tag, tags_ID_article)
                                                VALUES (:tag, :article)');

            $sth->bindParam(':tag', $tag->id);
            $sth->bindParam(':article', $lastid);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                return '[Article:5]'.$e->getMessage();
            }
        }
        return (int)$lastid;
    }

    /**
     * Deletes article, its comments, comment thread and tag associations
     */
    public function Delete(): void
    {
        $dbh = Database::Get();

        try {
            $art = self::GetByID($this->id);
        } catch (Exception $e) {
            echo '[Article:6]'.$e->getMessage();
            return;
        }

        // Delete comment
        $sql = 'DELETE FROM comments WHERE comment_ID_thread = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $art->comments);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:7]'.$e->getMessage();
        }

        // Delete thread
        $sql = 'DELETE FROM comment_threads WHERE ID_thread = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $art->comments);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:8]'.$e->getMessage();
        }

        // Delete tag associations
        $sql = 'DELETE FROM article_has_tags WHERE tags_ID_article = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:9]'.$e->getMessage();
        }

        // Delete article itself
        $sql = 'DELETE FROM articles WHERE ID_article = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:10]'.$e->getMessage();
        }
    }

    /**
     * @return void
     */
    public function Edit(): void
    {
        $dbh = Database::Get();

        $sth = $dbh->prepare('UPDATE articles 
                                            SET title_article = :title,
                                                friendly_title_article = :friendly,
                                                body_article = :body,
                                                date_article = :date,
                                                excerpt_article = :excerpt,
                                                article_ID_category = :category,
                                                article_ID_user = :author
                                            WHERE ID_article = :id');

        $sth->bindParam(':title', $this->title);
        $sth->bindValue(':friendly', self::friendlify($this->title));
        $sth->bindParam(':body', $this->body);
        $sth->bindParam(':date', $this->date);
        $sth->bindParam(':excerpt', $this->excerpt);
        $sth->bindParam(':category', $this->category->id);
        $sth->bindParam(':author', $this->author->id);
        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:11]'.$e->getMessage();
        }

        // Remove tags
        $sth = $dbh->prepare('DELETE FROM article_has_tags WHERE tags_ID_article = :article');

        $sth->bindParam(':article', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Article:12]'.$e->getMessage();
        }

        // Add tags too!
        foreach ($this->tags as $tag) {
            $sth = $dbh->prepare('INSERT INTO article_has_tags (tags_ID_tag, tags_ID_article) VALUES (:tag, :article)');

            $sth->bindParam(':tag', $tag->id);
            $sth->bindParam(':article', $this->id);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                echo '[Article:13]'.$e->getMessage();
            }
        }
    }

    /**
     * Transforms an assoc array returned from database query into an Article object
     * @param array $article Takes an assoc array describing an Article
     * @return Article Returns a built Article object
     * @throws Exception
     */
    private static function Build($article): Article
    {
        $art = new self();
        $art->setId($article['ID_article'])
            ->setTitle($article['title_article'])
            ->setFurl($article['friendly_title_article'])
            ->setBody($article['body_article'])
            ->setDate($article['date_article'])
            ->setExcerpt($article['excerpt_article'])
            ->setCategory($article['article_ID_category'])
            ->setTagsByArticle($article['ID_article'])
            ->setAuthor($article['article_ID_user'])
            ->setComments($article['article_ID_thread']);
        return $art;
    }

    private static function friendlify(string $str): string
    {
        $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
        return $str;
    }
}
