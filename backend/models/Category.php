<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 16:39
 */

namespace Backend\Models;

use Fuse\Fuse;
use PDO;
use PDOException;
use RuntimeException;

/**
 * Class Category
 * @package Backend\Models
 * @property int $id
 * @property string $name
 * @property string $furl
 * @property string $description
 * @property string $picture
 */
class Category
{
    /* @var int */
    private $id;
    /* @var string */
    private $name;
    /* @var string */
    private $furl;
    /* @var string */
    private $description;
    /* @var string */
    private $picture;


    /**
     * @param int $id
     * @return Category
     */
    public function setId(int $id): Category
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): Category
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $furl
     * @return Category
     */
    public function setFurl(string $furl): Category
    {
        $this->furl = $furl;
        return $this;
    }

    /**
     * @param string $description
     * @return Category
     */
    public function setDescription(string $description): Category
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param string $picture
     * @return Category
     */
    public function setPicture(string $picture): Category
    {
        $this->picture = $picture;
        return $this;
    }


    public function &__get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        throw new RuntimeException('Setting properties without proper setters is not allowed');
    }

    public function __isset($name)
    {
        return isset($this->$name);
    }

    /**
     * Gets all categories that exist in the database
     * @return array Returns an array of categories or a string with an error
     */
    public static function GetAll(): array
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM categories ORDER BY name_category';
        $sth = $dbh->query($sql);

        $categories = $sth->fetchAll();

        foreach ($categories as $key => $cat) {
            $categories[$key] = self::Build($cat);
        }
        return $categories;
    }

    /**
     * Gets category by the specified ID number
     * @param int $id Takes an Integer that is the desired category ID
     * @return Category Returns a Category, or a String with an error
     */
    public static function GetByID(int $id = 1): Category
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM categories WHERE ID_category = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindValue(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Category:1]'.$e->getMessage();
        }

        $category = $sth->fetch(PDO::FETCH_ASSOC);
        return self::Build($category);
    }

    /**
     * Gets categories with titles that match the query
     * @param string $query Takes a query to match the results against
     * @param int $limit Takes a number of results to return
     * @return array Returns an array of results, ordered by how well they match
     */
    public static function Search(string $query, int $limit = 5): array
    {
        $cats = json_decode(json_encode(self::GetAll()), true);

        $fuse = new Fuse($cats,
            [
                'keys' => ['name'],
                'includeScore' => true,
            ]
        );

        $r = $fuse->search($query);
        $rl = $limit > 0 ? array_chunk($r, $limit)[0] : $r;

        $out = [];
        foreach ($rl as $category) {
            $out[] = array(
                'id' => $category['item']['id'],
                'name' => $category['item']['name'],
                'description' => $category['item']['description'],
                'picture' => $category['item']['picture'],
                'furl' => 'https://sfnw.online/c/'.$category['item']['id'].'/'.$category['item']['furl'],
                'score' => $category['score']
            );
        }

        return $out;
    }

    /**
     * Deletes a category from the database, provided no articles have it
     * @return void
     */
    public function Delete(): void
    {
        $dbh = Database::Get();
        // Grab all articles and check if any has this category associated with it
        $sql = 'SELECT * FROM articles WHERE article_ID_category = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Category:2]'.$e->getMessage();
        }

        $articles = $sth->fetchAll();

        if (!empty($articles)) {
            echo count($articles) . ' articles with this category exist';
        }

        $sql = 'DELETE FROM categories WHERE ID_category = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Category:3]'.$e->getMessage();
        }
    }

    /** Adds Category to database
     * @return void
     */
    public function Add(): void
    {
        $dbh = Database::Get();
        // Prepare image placeholder
        $img = empty($this->picture) ? 'https://sfnw.online/public/assets/img/placeholder.png' : $this->picture;

        $sql = 'INSERT INTO categories (name_category, furl_category, description_category, picture_category) 
                VALUES (:name, :furl, :desc, :picture)';

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':name', $this->name);
        $sth->bindValue(':furl', self::friendlify($this->name));
        $sth->bindParam(':desc', $this->description);
        $sth->bindParam(':picture', $img);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Category:4]'.$e->getMessage();
        }
    }

    /**
     * Edits category
     * @return void
     */
    public function Edit(): void
    {
        $dbh = Database::Get();
        $sth = $dbh->prepare('UPDATE categories 
                                            SET name_category = :name,
                                                furl_category = :furl,
                                                description_category = :desc,
                                                picture_category = :picture
                                            WHERE ID_category = :id');

        $sth->bindParam(':name', $this->name);
        $sth->bindValue(':furl', self::friendlify($this->name));
        $sth->bindParam(':desc', $this->description);
        $sth->bindParam(':picture', $this->picture);
        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Category:5]'.$e->getMessage();
        }
    }

    /**
     * Counts categories currently in database
     * @return int Returns an Integer equal to the amount of categories, or -1 if error occurred
     */
    public static function Count(): int
    {
        $dbh = Database::Get();
        $sql = 'SELECT COUNT(*) FROM categories';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Category:6]'.$e->getMessage();
            return -1;
        }

        return (int)$sth->fetchColumn();
    }

    /**
     * Transforms an assoc array returned from database query into a Category object
     * @param array $category Takes an assoc array describing a Category
     * @return Category returns a built Category object
     */
    private static function Build(array $category): Category
    {
        $cat = new self();
        $cat->setId($category['ID_category'])
            ->setName($category['name_category'])
            ->setFurl($category['furl_category'])
            ->setDescription($category['description_category'])
            ->setPicture($category['picture_category']);
        return $cat;
    }

    private static function friendlify(string $str): string
    {
        $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
        return $str;
    }
}
