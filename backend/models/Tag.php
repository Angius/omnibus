<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 27.07.2018
 * Time: 04:48
 */

namespace Backend\Models;

use Fuse\Fuse;
use PDO;
use PDOException;
use RuntimeException;

/**
 * Class Tag
 * @package Backend\Models
 * @property int $id
 * @property string $name
 * @property string $furl
 * @property string $description
 * @property string $picture
 */
class Tag {
    /* @var int */
    private $id;
    /* @var string */
    private $name;
    /* @var string */
    private $furl;
    /* @var string */
    private $description;
    /* @var string */
    private $picture;

    /**
     * @param int $id
     * @return Tag
     */
    public function setId(int $id): Tag
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return Tag
     */
    public function setName(string $name): Tag
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $furl
     * @return Tag
     */
    public function setFurl(string $furl): Tag
    {
        $this->furl = $furl;
        return $this;
    }

    /**
     * @param string $description
     * @return Tag
     */
    public function setDescription(string $description): Tag
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param string $picture
     * @return Tag
     */
    public function setPicture(string $picture): Tag
    {
        $this->picture = $picture;
        return $this;
    }


    public function &__get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        throw new RuntimeException('Setting properties without proper setters is not allowed');
    }

    public function __isset($name)
    {
        return isset($this->$name);
    }


    /**
     * Gets all tags assigned to a selected article
     * @param int $art Takes an Integer that is the desired **article** ID
     * @return array Returns an array of tags or a string with an error
     * @throws PDOException
     */
    public static function GetByArticle(int $art): array
    {
        $dbh = Database::Get();

        $sql = 'SELECT tags.*, article_has_tags.*
                    FROM article_has_tags
                        JOIN tags ON article_has_tags.tags_ID_tag = tags.ID_tag
                    WHERE tags_ID_article = :article 
                    ORDER BY name_tag';

        $sth = $dbh->prepare($sql);
        $sth->bindParam(':article', $art);

        $sth->execute();

        $tags = $sth->fetchAll();

        foreach ($tags as $key => $t) {
            $tags[$key] = self::Build($t);
        }
        return $tags;
    }

    /**
     * Gets all tags that exist in the database
     * @param int $limit Takes a limit of how many results should be returned
     * @return array Returns an array of tags or a string with an error
     * @throws PDOException
     */
    public static function GetAll(int $limit = 0): array
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM tags ORDER BY name_tag';

        // Add limit if necessary
        $sql = $limit > 0 ? $sql.' LIMIT :l' : $sql;
        $li = (int)trim($limit);

        $sth = $dbh->prepare($sql);

        if($limit > 0) {
            $sth->bindParam('l', $li, PDO::PARAM_INT);
        }

        $sth->execute();

        $tags = $sth->fetchAll();

        foreach ($tags as $key => $t) {
            $tags[$key] = self::Build($t);
        }
        return $tags;
    }

    /**
     * Gets tag by the specified ID number
     * @param int $id Takes an Integer that is the desired tag ID
     * @return Tag Returns a Tag, or a String with an error
     */
    public static function GetByID(int $id): Tag
    {
        $dbh = Database::Get();

        $sql = 'SELECT * FROM tags WHERE ID_tag = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Tag:1]'.$e->getMessage();
        }

        $tag = $sth->fetch(PDO::FETCH_ASSOC);
        return self::Build($tag);
    }

    /**
     * Gets tags with titles that match the query
     * @param string $query Takes a query to match the results against
     * @param int $limit Takes a number of results to return
     * @return array Returns an array of results, ordered by how well they match
     */
    public static function Search(string $query, int $limit = 5): array
    {
        $tags = json_decode(json_encode(self::GetAll()), true);

        $fuse = new Fuse($tags,
            [
                'keys' => ['name'],
                'includeScore' => true,
            ]
        );

        $r = $fuse->search($query);
        $rl = $limit > 0 ? array_chunk($r, $limit)[0] : $r;

        $out = [];
        foreach ($rl as $tag) {
            $out[] = array(
                'id'          => $tag['item']['id'],
                'name'        => $tag['item']['name'],
                'description' => $tag['item']['description'],
                'picture'     => $tag['item']['picture'],
                'furl'        => 'https://sfnw.online/t/'.$tag['item']['id'].'/'.$tag['item']['furl'],
                'similarity'  => $tag['score'],
            );
        }

        return $out;
    }


    /**
     * Deletes tag and its associations with articles from database
     * @return void Returns null if successful or an error string
     */
    public function Delete(): void
    {
        $dbh = Database::Get();
        $sql = 'DELETE FROM article_has_tags WHERE tags_ID_tag = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $this->id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Tag:2]'.$e->getMessage();
        }

        $sql = 'DELETE FROM tags WHERE ID_tag = :id';
        $sth = $dbh->prepare($sql);
        $sth->bindParam(':id', $this->id);
        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Tag:3]'.$e->getMessage();
        }
    }

    /** Adds Tag to database
     * @return void
     */
    public function Add(): void
    {
        $dbh = Database::Get();
        // Prepare image placeholder
        $img = empty($this->picture) ? 'https://sfnw.online/public/assets/img/placeholder.png' : $this->picture;

        // If everything's fine, add this shit to the database
        $sql = 'INSERT INTO tags (name_tag, furl_tag, description_tag, picture_tag) 
                VALUES (:name, :furl, :desc, :picture)';

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':name', $this->name);
        $sth->bindValue(':furl', self::friendlify($this->name));
        $sth->bindParam(':desc', $this->description);
        $sth->bindParam(':picture', $img);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Tag:4]'.$e->getMessage();
        }
    }

    /**
     * Edits tag
     * @return void
     */
    public function Edit(): void
    {
        $dbh = Database::Get();
        // If everything's fine, add this shit to the database
        $sth = $dbh->prepare('UPDATE tags 
                                            SET name_tag = :name,
                                                furl_tag = :furl,
                                                description_tag = :desc,
                                                picture_tag = :picture
                                            WHERE ID_tag = :id');

        $sth->bindParam(':name', $this->name);
        $sth->bindValue(':furl', self::friendlify($this->name));
        $sth->bindParam(':desc', $this->description);
        $sth->bindParam(':picture', $this->picture);
        $sth->bindParam(':id', $this->id);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Tag:5]'.$e->getMessage();
        }
    }

    /**
     * Counts tags currently in database
     * @return int|string Returns an Integer equal to the amount of tags, or a String with an error
     */
    public static function Count() {
        $dbh = Database::Get();
        $sql = 'SELECT COUNT(*) FROM tags';
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            echo '[Tag:6]'.$e->getMessage();
        }

        return (int)$sth->fetchColumn();
    }

    /**
     * Transforms an assoc array returned from database query into a Tag object
     * @param array $tag Takes an assoc array describing a Tag
     * @return Tag returns a built Tag object
     */
    protected static function Build(array $tag): Tag
    {
        $t = new self();
        $t->setId($tag['ID_tag'])
          ->setName($tag['name_tag'])
          ->setFurl($tag['furl_tag'])
          ->setDescription($tag['description_tag'])
          ->setPicture($tag['picture_tag']);
        return $t;
    }

    private static function friendlify(string $str): string
    {
        $str = preg_replace('~[^\\pL0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~', '', $str);
        return $str;
    }
}
