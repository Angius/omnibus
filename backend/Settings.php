<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 29.09.2018
 * Time: 01:28
 */

namespace Backend;

class Settings
{
    public static function Read(string $setting): string
    {
        $path = dirname(__DIR__);
        $config = parse_ini_file($path . '/config.ini');
        return $config[$setting];
    }
}
