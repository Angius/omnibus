<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 01.10.2018
 * Time: 04:29
 */

use Backend\Models\Category;
use Backend\Models\Classroom;
use Backend\Models\Discord;
use Backend\Models\Tag;
use Backend\Models\User;
use Backend\Token;
use Backend\TwigHandler;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

$message = null;
if (isset($_GET['del'], $_GET['t']) && !empty($_GET['del']) && !empty($_GET['t'])) {
    // Check CSRF
    if ($_GET['t'] === $_SESSION['token']) {
        $message = Classroom::Delete($_GET['del']) ?? 'Class successfully deleted';
    } else {
        die('Unauthorized access');
    }
}

// Load up Twig stuff
require dirname(__DIR__, 4).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/admin/managers/classes.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),

        'token'         => $token,
        'message'       => $message,
    ));

    // Handle all possible errors
} catch (LoaderError $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (RuntimeError $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (SyntaxError $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
