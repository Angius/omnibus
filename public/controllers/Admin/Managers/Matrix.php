<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 15.10.2018
 * Time: 05:14:01
 */

use Backend\Models\Article;
use Backend\Models\Category;
use Backend\Models\Discord;
use Backend\Models\Tag;
use Backend\Models\User;
use Backend\Token;
use Backend\TwigHandler;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

$message = null;
if (isset($_POST['token']) && !empty($_POST['token'])) {
    // Check CSRF
    if ($_POST['token'] === $_SESSION['token']) {
        if (!empty($_POST['id']) && isset($_POST['id'])) {
            $art = Article::GetByID($_POST['id']);
            $art->setCategory($_POST['category'])
                ->setTags($_POST['tags'])
                ->setAuthor($_POST['author'])
                ->Edit();
        }
    } else {
        die('Unauthorized access');
    }
}

// Load up Twig stuff
require dirname(__DIR__, 4).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('admin/managers/matrix.twig', array(
        'theme'      => $theme = $_COOKIE['theme'] ?? 'light',
        'user'       => !empty($_SESSION['userid']) ? User::GetByID($_SESSION['userid']) : null,
        'discord_users' => Discord::Online(),

        'token'      => $token,
        'articles'   => Article::GetAll(),
        'categories' => Category::GetAll(),
        'tags'       => Tag::GetAll(),
        'users'      => User::GetAll(),
    ));

    // Handle all possible errors
} catch (LoaderError $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (RuntimeError $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (SyntaxError $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}

