<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 06.10.2018
 * Time: 07:26
 */

use Backend\Models\Category;
use Backend\Models\Classroom;
use Backend\Models\Discord;
use Backend\Models\Tag;
use Backend\Models\User;
use Backend\Token;
use Backend\TwigHandler;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

require dirname(__DIR__,4).'/includes/friendlify.php';

$id = $id ?? $_POST['id'] ?? null;

$message = null;
if (isset($_POST['token']) && !empty($_POST['token'])) {
    // Check CSRF
    if ($_POST['token'] === $_SESSION['token']) {
        if (!empty($_POST['id']) && isset($_POST['id'])) {
            $dbg = Classroom::Edit(
                $id,
                $_POST['open-date'],
                $_POST['close-date'],
                $_POST['open-title'],
                $_POST['close-title'],
                $_POST['open-body'],
                $_POST['close-body']
            );
        } else {
            $dbg = Classroom::Add(
                $_POST['open-date'],
                $_POST['close-date'],
                $_POST['open-title'],
                $_POST['close-title'],
                $_POST['open-body'],
                $_POST['close-body']
            );
        }
    } else {
        die('Unauthorized access');
    }
}

// Load up Twig stuff
require dirname(__DIR__, 4).'/backend/TwigHandler.php';
$twig = TwigHandler::Get();

// Set up variables
$token = Token::Get(64);
$_SESSION['token'] = $token;

// Render Twig template
try {
    // Render the actual Twig template
    echo $twig->render('/admin/editors/class.twig', array(
        'theme'         => $theme = $_COOKIE['theme'] ?? 'light',
        'user'          => User::GetByID($_SESSION['userid']),
        'discord_users' => Discord::Online(),
        'classes'       => Classroom::GetAll(),
        'tags'          => Tag::GetAll(),
        'categories'    => Category::GetAll(),
        'authors'       => User::GetAllByRole(false, true, true),

        'class'         => $id === null || $id === '' ? null : Classroom::GetByID($id),
        'token'         => $token,
        'message'       => $message,
    ));

    // Handle all possible errors
} catch (LoaderError $e) {
    header('Content-type: application/json');
    echo json_encode('Error [1]: '.$e);
} catch (RuntimeError $e) {
    header('Content-type: application/json');
    echo json_encode('Error [2]: '.$e);
} catch (SyntaxError $e) {
    header('Content-type: application/json');
    echo json_encode('Error [3]: '.$e);
}
