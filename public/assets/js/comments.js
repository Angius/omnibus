getComments();

let form = document.querySelector('#comment_form');
let container = document.getElementById('display_comment');
let html = '';
let reports = document.querySelectorAll('.report-btn');
let area = document.getElementById('comment');
let counter = document.querySelector('.char-counter');
let maxchars = 1000;
console.log(counter);

area.addEventListener('input', function (e) {
    console.log(area.value.length);
    counter.innerHTML = area.value.length + '/' + maxchars;
});

form.addEventListener('submit', function (e) {
    e.preventDefault();

    const params = new URLSearchParams();
    params.append('body', document.getElementById('comment').value);
    params.append('thread', document.getElementById('thread').value);
    params.append('token', token);

    axios.post('/api/comments', params)
    .then(function (data) {
        getComments();
    })
    .catch(error => console.error(error));
});

function postData(s, param2) {

}

function attachReports() {
    reports = document.querySelectorAll('.report-btn');

    console.log(reports.length);
    for (let i = 0; i < reports.length; i++) {
        reports[i].addEventListener("click", function (e) {
            reportComment(i);
        })
    }
}

function getComments() {
    let thread = document.getElementById("thread").value;

    axios.get('/api/comments',
        {
            params:
                {
                    thread: thread
                }
        }
    ).then(function (response) {
        container.innerHTML = response.data;
        attachReports();
    }).catch(error => console.error(error));
}

function reportComment(id) {
    const params = new URLSearchParams();
    params.append('comment', reports[id].dataset.id);
    params.append('token', document.getElementById('comment-token').value);

    axios.post('/api/comments/report', params)
         .then(function (data) {
             getComments();
         })
         .catch(error => console.error(error));
}
