let buttons = document.querySelectorAll('.shelf-btn');
let token = document.getElementById('token').dataset.token;

for (let i = 0; i < buttons.length; i++) {
    let icon = buttons[i].querySelector("i");

    buttons[i].addEventListener('click', function (e) {
        icon.classList.remove('heart', 'bookmark');
        icon.classList.add('loading', 'spinner');

        const params = new URLSearchParams();
        params.append('shelf', buttons[i].dataset.shelf);
        params.append('article', buttons[i].dataset.article);
        params.append('token', token);

        axios.post('/api/shelves', params)
            .then(function (response) {
                if (response.data.item === 'bookmark' && response.data.msg === 'added') {
                    icon.classList.add('green', 'bookmark');
                } else if (response.data.item === 'bookmark' && response.data.msg === 'removed') {
                    icon.classList.remove('green');
                    icon.classList.add('bookmark');
                } else if (response.data.item === 'favourite' && response.data.msg === 'added') {
                    icon.classList.add('red', 'heart');
                } else if (response.data.item === 'favourite' && response.data.msg === 'removed') {
                    icon.classList.remove('red');
                    icon.classList.add('heart');
                }
            icon.classList.remove('loading', 'spinner');
            });
    });
}
