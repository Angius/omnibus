$(document).ready(function(){
    $.getScript("/semantic/dist/semantic.min.js")
        .done(function( script, textStatus ) {

            if (typeof doneLoading === "function") {
                doneLoading();
            }

            $('.ui.search')
                .search({
                    apiSettings: {
                        url: 'https://sfnw.online/api/articles.php?search={query}&limit=8'
                    },
                    fields: {
                        title: 'title',
                        description: '',
                        id: 'id',
                        url: 'furl'
                    },
                    searchFields: [
                        'title'
                    ],
                    cache: false,
                    minCharacters: 3
                })
            ;

            $('.ui.sticky')
                .sticky({
                    content: '#content'
                })
            ;

            $('.ui.dropdown')
                .dropdown({
                    allowAdditions: true
                })
            ;

            $('.ui.accordion')
                .accordion()
            ;

            $('.menu .item')
                .tab()
            ;

            $('.message .close')
                .on('click', function () {
                    $(this)
                        .closest('.message')
                        .transition('fade')
                    ;
                })
            ;

            setTimeout(function () {
                $('.message.timed').transition('fade')
            }, 2500);

            $('#search-select')
                .dropdown({
                    allowAdditions: true
                })
            ;

            $('#multi-select')
                .dropdown({
                    allowAdditions: true
                })
            ;

            $('.ui.checkbox')
                .checkbox()
            ;


        })
        .fail(function( jqxhr, settings, exception ) {
            console.log(exception);
        });
});
