function prependClass(sel, strClass, last = true) {
    let $el = jQuery(sel);
    let classes = $el.attr('class');

    if (last) {
        /* prepend class */
        classes = strClass + ' ' + classes;
        $el.attr('class', classes);
    } else {
        classes = classes.substr(classes.indexOf(" ") + 1);
        $el.attr('class', classes);
    }
}
