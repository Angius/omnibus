# Omnibus

CMS for School for New Writers

## Changelog

### 2.0.0α - 07.05.2019

This is an alpha version of the next big update, it'still in development

- Changes in progress
  - Rewriting the controllers to match object-oriented style
  - Rewriting the routing to use `controller#action` scheme
  - Rewriting the models to comply with best object-oriented practices
  - Rethinking the architecture for best security
  - Appeasing the almighty linter

### 1.4.0 – 28.09.2018

- Changes
  - Moved from Stylus to SASS, since Stylus has been abandoned
  - Moved from SemanticUI to FomanticUI, since SUI development slowed down bya lot
  - Moved from hand-made mess of ``.htaccess`` and shit to Twig & AltoRouter combo
- Todo
  - T&AR migration for:
    - user auth (registration, login, etc)
    - user profiles
    - admin panel
    - API

### 1.3.1 – 12.09.2018

- Changes
  - Added Matrix edit to edit tags, categories and authors of multiple articles at once
  - Swapped `/admin2/` route for `/admin/`

### 1.3.0 – 12.09.2018

- Changes
  - Admin panel has been redone
    - Now uses PHP routing instead of JS
    - Thanks to that, the back button, refreshing or even sharing links works
    - Should also increase security
  - That's the first step to implementing PHP routing instead of .htaccess routing everywhere
- Fixes
  - Fixed some lingering issues

### 1.2.0 – 09.09.2018

- Changes
  - Beautiful registration and password restore emails
  - Changed some obsolete procedural function usages for new, OO-based ones
  - Revamped user profile avatar editing to be much more secure (albeit a lil' bit uglier)
- Fixes
  - Odds and ends

### 1.1.3 – 08.09.2018

- Fixes
  - Fixed whatever the latest refactoring has broken
- Changes
  - Default sorting methods changed:
    - Articles: by date, newest to oldest
    - Categories and Tags: by name, A-Z
    - Users: by Role high to low, by name inside the role, A-Z

### 1.1.2 – 08.09.2018

- API upgrades
  - Added `limit` param to Articles and Tags to limit the amount of objects returned
  - Added `search` param to Articles endpoint to search in article titles

### 1.1.1 – 06.09.2018

- Further procedural -> OOP refactoring
  - Articles
  - Categories
  - Classes (now Classrooms due to Class being a reserved word)
  - Roles
  - Sitemap
  - Sources (used with Articles)
  - Tags
  - Users

### 1.1.0 – 05.09.2018

- Fixed some minor SUI-related fuckups
- Returned to SUI from FUI (waiting for bugfixes)
- Code refactoring from procedural to object-oriented:
  - database_controller.php -> Database class
  - category_controller.php -> Category class
  - quotes_controller.php -> Quote class
  - article_controller.php -> Article class (usages need to be changed)
