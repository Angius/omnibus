<?php

namespace Omnibus\Controller;

use Backend\Models\Category;
use Backend\Models\Classroom;
use Backend\Models\Discord;
use Backend\Models\Tag;
use Backend\Models\User;
use Backend\Token;
use Backend\TwigHandler;
use Exception;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Environment;

/**
 * Class Controller
 * Application controllers should extend this class.
 * @package Omnibus\Controller
 */
abstract class Controller
{
    /** @var Environment */
    private $twig;
    /** @var null|User */
    private $user;
    /** @var string */
    private $token;
    /** @var array */
    private $base_data = [];
    /** @var Session */
    protected $session;

    public function __construct(Session $session)
    {
        $this->session = $session;

        $this->twig  = TwigHandler::Get();
        try {
            $this->user = $this->session->has('userid') ? User::GetByID($this->session->get('userid')) : null;
        } catch (Exception $e) {
            echo '[BaseController:1]'.$e->getMessage();
        }
        $this->token = Token::Get(32);
    }


    /**
     * Renders the given data
     * @param string $template The template name
     * @param array $data The data to render as an associative array
     */
    protected function render($template, $data): void
    {
        // Append twig template extension
        $template .= '.twig';

        try {
            // Render the actual Twig template
            echo $this->twig->render($template, array_merge($this->base_data, $data));

        } catch (LoaderError $e) {
            header('Content-type: application/json');
            echo json_encode('Error [1]: ' . $e);
        } catch (RuntimeError $e) {
            header('Content-type: application/json');
            echo json_encode('Error [2]: ' . $e);
        } catch (SyntaxError $e) {
            header('Content-type: application/json');
            echo json_encode('Error [3]: ' . $e);
        }

        $this->session->set('token', $this->token);
    }

    /**
     * Sets base data to be displayed by most views
     * @throws Exception
     */
    public function setBaseData(): void
    {
        $this->base_data  = [
            'theme' => $_COOKIE['theme'] ?? 'light',
            'token' => $this->getToken(),
            'user'  => $this->getUser(),

            'discord_users' => Discord::Online(),
            'classes'       => Classroom::GetAll(),
            'tags'          => Tag::GetAll(),
            'categories'    => Category::GetAll(),
            'authors'       => User::GetAllByRole(false, true, true),
        ];
    }

    /**
     * Returns the current user
     * @return null|string|User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Returns generated token
     * @return string
     */
    private function getToken(): string
    {
        return $this->token;
    }
}
