<?php

namespace Omnibus\Application;

use AltoRouter;
use Backend\Models\Role;
use Backend\Models\User;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class Application
{
    /** @var AltoRouter */
    protected $router;
    /** @var Run */
    protected $whoops;
    /** @var null|User */
    protected $user;
    /** @var null|Role */
    protected $role;
    /** @var Session */
    public $session;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->router = new AltoRouter();
    }

    public function run(): void
    {
        $this->bootstrap();
        $this->addRoutes();
        $this->handleRequest();
    }

    private function handleRequest(): void
    {
        /* *********************************************** *\
        |*                  ROUTE HANDLING                 *|
        \* *********************************************** */
        $match = $this->router->match();

        // Handle routing
        if ($match) {
            $target = ((array)$match)['target'];
            if (is_callable($target)) {
                call_user_func_array(((array)$match)['target'], ((array)$match)['params']);
            } else {
                $parts = explode('#', $target);
                if (count($parts) === 2) {
                    // instantiate a new controller
                    $controller = new $parts[0]($this->session);
                    // run the controller's method
                    $controller->{$parts[1]}(((array)$match)['params']);
                }
            }
        } else {
            // no route was matched
            header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
            die('404');
        }
    }

    protected function addRoutes(): void
    {

        /* *********************************************** *\
        |*                  ROUTE MAPPINGS                 *|
        \* *********************************************** */

        /***************************************************************\
        |*                    PUBLIC ACCESS ROUTES                     *|
        |*             Parts of the site anyone can access             *|
        \***************************************************************/
        try {
            $this->router->addRoutes([
                //Home
                ['GET', '/', 'Application\\Controller\\HomeController#index'],
                // Article
                ['GET', '/article/[i:id]/[f:furl]?', 'Application\\Controller\\ArticleController#index'],
                ['GET', '/a/[:id]/[f:furl]?', 'Application\\Controller\\ArticleController#index'],
                // Articles
                ['GET', '/articles/[:page]?', 'Application\\Controller\\ArticlesController#index'],
                // Categories
                ['GET', '/categories', 'Application\\Controller\\CategoriesController#index'],
                ['GET', '/category/[i:id]/[f:furl]?', 'Application\\Controller\\CategoriesController#index'],
                ['GET', '/c/[:id]/[f:furl]?', 'Application\\Controller\\CategoriesController#index'],
                // Tags
                ['GET', '/tags', 'Application\\Controller\\TagsController#index'],
                ['GET', '/tag/[i:id]/[f:furl]?', 'Application\\Controller\\TagsController#index'],
                ['GET', '/t/[i:id]/[f:furl]?', 'Application\\Controller\\TagsController#index'],
                // Authors
                ['GET', '/authors', 'Application\\Controller\\AuthorsController#index'],
                ['GET', '/author/[i:id]/[f:furl]?', 'Application\\Controller\\AuthorsController#index'],
                ['GET', '/au/[i:id]/[f:furl]?', 'Application\\Controller\\AuthorsController#index'],
                // Classes
                ['GET', '/class/[i:id]?', 'Application\\Controller\\ClassController#index'],

                // USERS
                // Login
                ['GET', '/login', 'Application\\Controller\\User\\LoginController#index'],
                ['POST', '/login', 'Application\\Controller\\User\\LoginController#login'],
                // Logout
                ['GET', '/logout', 'Application\\Controller\\User\\LoginController#logout'],
                // Register
                ['GET', '/register', 'Application\\Controller\\User\\RegisterController#index'],
                ['POST', '/register', 'Application\\Controller\\User\\RegisterController#register'],
                // Activate
                ['GET', '/activate/[a:code]', 'Application\\Controller\\User\\ActivateController#activate'],
                ['GET', '/activate', 'Application\\Controller\\User\\ActivateController#index'],
                ['POST', '/activate', 'Application\\Controller\\User\\ActivateController#activate'],
                // Restore
                ['GET', '/restore', 'Application\\Controller\\User\\RestoreController#index'],
                ['POST', '/restore', 'Application\\Controller\\User\\RestoreController#restore'],

                // RSS
                ['GET', '/RSS', 'Application\\Controller\\RssController#index'],
            ]);
        } catch (Exception $e) {
            echo $e;
        }

        /***************************************************************\
         *                          PUBLIC API                          *
         *                     Public API endpoints                     *
        \***************************************************************/
        try {
            $this->router->addRoutes([
                // Articles
                ['GET', '/articles.json', 'Application\\Controller\\API\\ArticlesApiController#index'], // All
                ['GET', '/article/[i:id].json', 'Application\\Controller\\API\\ArticleApiController#index'], // Single
                ['GET', '/articles/search.json', 'Application\\Controller\\API\\ArticlesSearchController#index'], // Search
            ]);
        } catch (Exception $e) {
            echo $e;
        }

        /***************************************************************\
         *                        INTERNAL API                          *
         *     API for use by the internal systems, CSRF-protected      *
        \***************************************************************/
        if ($this->user !== null) {
            try {
                $this->router->addRoutes([
                    //Shelves
                    ['POST', '/api/shelves', 'Application\\Controller\\API\\Internal\\InternalApiShelvesController#index'],
                    // Comments
                    ['GET', '/api/comments', 'Application\\Controller\\API\\Internal\\InternalApiCommentsController#get'],
                    ['POST', '/api/comments', 'Application\\Controller\\API\\Internal\\InternalApiCommentsController#add'],
                    ['POST', '/api/comments/report', 'Application\\Controller\\API\\Internal\\InternalApiCommentsController#report'],
                ]);
            } catch (Exception $e) {
                echo $e;
            }
        }

        /***************************************************************\
         *                        USER-ONLY AREA                        *
         *          Area only registered users have access to           *
        \***************************************************************/
        if ($this->user !== null) {
            try {
                $this->router->addRoutes([
                    //User's own profile
                    ['GET', '/profile', 'Application\\Controller\\User\\Profile\\ProfileController#indexSelf'],
                    // User profiles
                    ['GET', '/user/[i:id]/[f:furl]?', 'Application\\Controller\\User\\Profile\\ProfileController#indexUser'],
                    ['GET', '/u/[i:id]/[f:furl]?', 'Application\\Controller\\User\\Profile\\ProfileController#indexUser'],
                    // Profile edit
                    ['GET', '/profile/update', 'Application\\Controller\\User\\Profile\\UpdateController#index'],
                    ['POST', '/profile/update', 'Application\\Controller\\User\\Profile\\UpdateController#update'],
                ]);
            } catch (Exception $e) {
                echo $e;
            }
        }

        /***************************************************************\
         *                     ADMIN-ONLY ROUTES                        *
         *  Parts of the site only those with role->is_admin can access *
        \***************************************************************/
        if ($this->role !== null && $this->role->is_admin) {
            try {
                $this->router->addRoutes([
                    // ADMIN
                    ['GET', '/admin', 'Application\\Controller\\Admin\\DashboardController#index'],
                    // Managers
                    ['GET', '/admin/articles', 'Application\\Controller\\Admin\\Managers\\ArticlesManagerController#index'],
                    ['GET', '/admin/users', 'Application\\Controller\\Admin\\Managers\\UsersManagerController#index'],
                    ['GET', '/admin/categories', 'Application\\Controller\\Admin\\Managers\\CategoriesManagerController#index'],
                    ['GET', '/admin/tags', 'Application\\Controller\\Admin\\Managers\\TagsManagerController#index'],
                    ['GET', '/admin/classes', static function () {require 'public/controllers/Admin/Managers/Classes.php';}],
                    ['GET', '/admin/comments', static function () {require 'public/controllers/Admin/Managers/Comments.php';}],
                    // Editors
                    ['GET', '/admin/articles/edit/[i:id]?', 'Application\\Controller\\Admin\\Editors\\ArticleEditorController#index'],
                    ['POST', '/admin/articles/edit', 'Application\\Controller\\Admin\\Editors\\ArticleEditorController#add'],
                    ['GET', '/admin/categories/edit/[i:id]?', 'Application\\Controller\\Admin\\Editors\\CategoryEditorController#index'],
                    ['POST', '/admin/categories/edit', 'Application\\Controller\\Admin\\Editors\\CategoryEditorController#add'],
                    ['GET', '/admin/tags/edit/[i:id]?', 'Application\\Controller\\Admin\\Editors\\TagEditorController#index'],
                    ['POST', '/admin/tags/edit', 'Application\\Controller\\Admin\\Editors\\TagEditorController#add'],
                    ['GET', '/admin/users/edit/[i:id]?', 'Application\\Controller\\Admin\\Editors\\UserEditorController#index'],
                    ['POST', '/admin/users/edit', 'Application\\Controller\\Admin\\Editors\\UserEditorController#add'],
                    ['GET', '/admin/classes/edit/[i:id]?', static function (int $id = null) {$id;require 'public/controllers/Admin/Editors/Class.php';}],
                    ['POST', '/admin/classes/edit', static function () {require 'public/controllers/Admin/Editors/Class.php';}],

                    // Deleters
                    ['GET', '/admin/articles/delete', 'Application\\Controller\\Admin\\Deleters\\ArticleDeleteController#index'],
                    ['GET', '/admin/categories/delete', 'Application\\Controller\\Admin\\Deleters\\CategoryDeleteController#index'],
                    ['GET', '/admin/tags/delete', 'Application\\Controller\\Admin\\Deleters\\TagDeleteController#index'],
                    ['GET', '/admin/users/delete', 'Application\\Controller\\Admin\\Deleters\\UserDeleteController#index'],

                ]);
            } catch (Exception $e) {
                echo $e;
            }
        }

        /***************************************************************\
         *                       ROUTE TO MATRIX                        *
         *  Parts of the site only those with role->is_admin can access *
        \***************************************************************/
        if ($this->role !== null && $this->role->matrix_access) {
            try {
                $this->router->addRoutes([
                    // Matrix
                    ['GET|POST', '/admin/articles/matrix', static function () {
                        require 'public/controllers/Admin/Managers/Matrix.php';
                    }],
                ]);
            } catch (Exception $e) {
                echo $e;
            }
        }
    }

    protected function bootstrap(): void
    {
        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);

        $sessionStorage = new NativeSessionStorage([
            'cookie_httponly' => true,
            'cookie_samesite' => 'Strict',
            'cookie_secure'   => true,
        ], new NativeFileSessionHandler());
        $this->session = new Session($sessionStorage);
        $this->session->start();

        // Set up router
        $this->router->setBasePath('');
        $this->router->addMatchTypes(['f' => '[a-zA-Z0-9\-]++']);

        // Set up Whoops
        $this->whoops = new Run;
        $this->whoops->pushHandler(new PrettyPageHandler);
        $this->whoops->register();

        // Check RememberMe
        $remember_cookie = $_COOKIE['__Secure-rememberme'] ?? '';
        if ($remember_cookie) {
            [$user, $token, $mac] = explode(':', $remember_cookie);
            if (!password_verify($user . ':' . $token, $mac)) {
                die('Invalid!');
            }
            $usertoken = User::GetRememberMe((int)$user);
            if (hash_equals($usertoken, $token)) {
                $this->session->set('userid', $user);
            }
        }

        // Get user
        try {
            $this->user = $this->session->has('userid') ? User::GetByID($this->session->get('userid')) : null;
        } catch (Exception $e) {
            echo '[Application:1]'.$e->getMessage();
        }
        // Get user role
        $this->role = ($this->session->has('userid') && $this->user !== null) ? $this->user->role : null;

        // Update last seen time
        if ($this->session->has('userid') && $this->user !== null) {
            try {
                $this->user->UpdateLastSeen();
            } catch (Exception $e) {
                echo '[Application:2]'.$e->getMessage();
            }
        }
    }
}
