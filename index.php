<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 28.09.2018
 * Time: 03:03
 */

require 'vendor/autoload.php';

use Omnibus\Application\Application;

define('IS_DEV', true);

$application = new Application();
$application->run();
