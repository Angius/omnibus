<?php
/**
 * Created by PhpStorm.
 * User: Angius
 * Date: 28.07.2018
 * Time: 05:44
 */

use Backend\Models\Sitemap;

$log = fopen('log.txt', 'ab') or die('Unable to open file!');

// If a callback from API
if (!empty($_POST)) {
    // Get sitemap string from post
    $sitemap_url = json_decode($_POST['data'], true)['response'];
    $sitemap = fopen($sitemap_url, 'rb');

    // Create sitemap.xml
    file_put_contents('../sitemap.xml', $sitemap);

    // Create sitemap.xml.gz
    $gzdata = gzencode($sitemap, 9);
    file_put_contents('../sitemap.xml.gz', $gzdata);

    // Save response in case it's an error
    file_put_contents('lastresponse.txt', var_export($_POST, true) . "\r\n");

    // Log creation to file
    fwrite($log, date('r') . "\r\n----\r\n");
    fclose($log);

    die();
}

// Get Codepunker token from ini
$path = dirname(__DIR__, 2);
$config = parse_ini_file($path.'/config.ini');

$key = $config['codepunker-key'];

// Authorize Codepunker API
$auth = Sitemap::Authorize($key);
if ($auth === '' || $auth === null) { fwrite($log, "Sitemap auth failed!\r\n"); }
$auth = json_decode($auth, true);

$response = $auth['response'];

// Send a POST request
$sitemap = Sitemap::GetSitemap($response, 'https://sfnw.online/cron/runsitemap.php');
if ($sitemap === '' || $sitemap === null) { fwrite($log, "Sitemap gen failed!\r\n"); }

// Log sitemap request to file
fwrite($log, date('r') . "\r\n");
fclose($log);
