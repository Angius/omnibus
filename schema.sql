-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 07, 2019 at 07:49 PM
-- Server version: 5.7.18
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `angius_omnibus`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
                            `ID_article` int(11) NOT NULL,
                            `title_article` tinytext NOT NULL,
                            `friendly_title_article` varchar(100) NOT NULL,
                            `body_article` longtext NOT NULL,
                            `date_article` datetime NOT NULL,
                            `excerpt_article` varchar(1000) NOT NULL,
                            `article_ID_category` int(11) NOT NULL,
                            `article_ID_user` int(11) NOT NULL,
                            `article_ID_thread` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `article_has_tags`
--

CREATE TABLE `article_has_tags` (
                                    `tags_ID_tag` int(11) NOT NULL,
                                    `tags_ID_article` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE `bookmarks` (
                             `bookmark_ID_user` int(11) NOT NULL,
                             `bookmark_ID_article` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
                              `ID_category` int(11) NOT NULL,
                              `name_category` varchar(45) NOT NULL,
                              `furl_category` varchar(45) NOT NULL,
                              `picture_category` varchar(255) DEFAULT NULL,
                              `description_category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
                           `ID_class` int(11) NOT NULL,
                           `date_start_class` date NOT NULL,
                           `date_end_class` date NOT NULL,
                           `title_start_class` varchar(200) NOT NULL,
                           `title_end_class` varchar(200) NOT NULL,
                           `body_start_class` varchar(10000) NOT NULL,
                           `body_end_class` varchar(10000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
                            `ID_comment` int(45) NOT NULL,
                            `comment_ID_thread` int(11) NOT NULL,
                            `comment_ID_user` int(11) NOT NULL,
                            `body_comment` varchar(1000) NOT NULL,
                            `date_comment` datetime NOT NULL,
                            `reported_by_comment` int(11) DEFAULT NULL,
                            `report_date_comment` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `comment_threads`
--

CREATE TABLE `comment_threads` (
                                   `ID_thread` int(11) NOT NULL,
                                   `source_thread` enum('article','class','profile','','','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
                              `favourite_ID_user` int(11) NOT NULL,
                              `favourite_ID_article` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
                           `ID_report` int(11) NOT NULL,
                           `user_report` int(11) NOT NULL,
                           `comment_report` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
                         `ID_role` int(11) NOT NULL,
                         `name_role` varchar(45) NOT NULL,
                         `is_admin` tinyint(1) NOT NULL DEFAULT '0',
                         `is_author` tinyint(1) NOT NULL DEFAULT '0',
                         `matrix_access` tinyint(1) NOT NULL DEFAULT '0',
                         `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
                        `ID_tag` int(11) NOT NULL,
                        `name_tag` varchar(45) NOT NULL,
                        `furl_tag` varchar(45) NOT NULL,
                        `description_tag` tinytext NOT NULL,
                        `picture_tag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
                         `ID_user` int(11) NOT NULL,
                         `name_user` varchar(25) NOT NULL,
                         `bio_user` text,
                         `title_user` varchar(40) DEFAULT NULL,
                         `link_user` varchar(128) DEFAULT NULL,
                         `avatar_user` varchar(255) DEFAULT NULL,
                         `email_user` varchar(45) NOT NULL,
                         `joined_user` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `last_seen_user` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `password` varchar(255) NOT NULL,
                         `code_user` varchar(32) NOT NULL,
                         `activated_user` tinyint(1) NOT NULL DEFAULT '0',
                         `muted_user` datetime DEFAULT NULL,
                         `user_ID_role` int(11) NOT NULL,
                         `thread_ID_user` int(11) DEFAULT NULL,
                         `remember_me` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
    ADD PRIMARY KEY (`ID_article`,`article_ID_category`,`article_ID_user`) USING BTREE,
    ADD KEY `fk_articles_categories1_idx` (`article_ID_category`),
    ADD KEY `fk_articles_users1_idx` (`article_ID_user`),
    ADD KEY `article_ID_thread` (`article_ID_thread`);
ALTER TABLE `articles` ADD FULLTEXT KEY `title_article` (`title_article`);

--
-- Indexes for table `article_has_tags`
--
ALTER TABLE `article_has_tags`
    ADD KEY `fk_tags_has_articles_articles1_idx` (`tags_ID_article`),
    ADD KEY `fk_tags_has_articles_tags_idx` (`tags_ID_tag`);

--
-- Indexes for table `bookmarks`
--
ALTER TABLE `bookmarks`
    ADD PRIMARY KEY (`bookmark_ID_user`,`bookmark_ID_article`),
    ADD KEY `fk_users_has_articles_articles1_idx` (`bookmark_ID_article`),
    ADD KEY `fk_users_has_articles_users1_idx` (`bookmark_ID_user`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
    ADD PRIMARY KEY (`ID_category`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
    ADD PRIMARY KEY (`ID_class`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
    ADD PRIMARY KEY (`ID_comment`),
    ADD KEY `fk_articles_has_users_users1_idx` (`comment_ID_user`),
    ADD KEY `fk_articles_has_users_articles1_idx` (`comment_ID_thread`);

--
-- Indexes for table `comment_threads`
--
ALTER TABLE `comment_threads`
    ADD PRIMARY KEY (`ID_thread`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
    ADD PRIMARY KEY (`favourite_ID_user`,`favourite_ID_article`),
    ADD KEY `fk_users_has_articles_articles2_idx` (`favourite_ID_article`),
    ADD KEY `fk_users_has_articles_users2_idx` (`favourite_ID_user`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
    ADD PRIMARY KEY (`ID_report`),
    ADD KEY `user_report` (`user_report`),
    ADD KEY `comment_report` (`comment_report`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
    ADD PRIMARY KEY (`ID_role`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
    ADD PRIMARY KEY (`ID_tag`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`ID_user`,`user_ID_role`),
    ADD UNIQUE KEY `fk_thread` (`thread_ID_user`),
    ADD KEY `fk_user_role1_idx` (`user_ID_role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
    MODIFY `ID_article` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
    MODIFY `ID_category` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
    MODIFY `ID_class` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
    MODIFY `ID_comment` int(45) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comment_threads`
--
ALTER TABLE `comment_threads`
    MODIFY `ID_thread` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
    MODIFY `ID_report` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
    MODIFY `ID_role` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
    MODIFY `ID_tag` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `ID_user` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
    ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`article_ID_thread`) REFERENCES `comment_threads` (`ID_thread`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_articles_categories1` FOREIGN KEY (`article_ID_category`) REFERENCES `categories` (`ID_category`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_articles_users1` FOREIGN KEY (`article_ID_user`) REFERENCES `users` (`ID_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `article_has_tags`
--
ALTER TABLE `article_has_tags`
    ADD CONSTRAINT `fk_tags_has_articles_articles1` FOREIGN KEY (`tags_ID_article`) REFERENCES `articles` (`ID_article`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_tags_has_articles_tags` FOREIGN KEY (`tags_ID_tag`) REFERENCES `tags` (`ID_tag`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bookmarks`
--
ALTER TABLE `bookmarks`
    ADD CONSTRAINT `fk_users_has_articles_articles1` FOREIGN KEY (`bookmark_ID_article`) REFERENCES `articles` (`ID_article`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_users_has_articles_users1` FOREIGN KEY (`bookmark_ID_user`) REFERENCES `users` (`ID_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
    ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`comment_ID_thread`) REFERENCES `comment_threads` (`ID_thread`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_articles_has_users_users1` FOREIGN KEY (`comment_ID_user`) REFERENCES `users` (`ID_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `favourites`
--
ALTER TABLE `favourites`
    ADD CONSTRAINT `fk_users_has_articles_articles2` FOREIGN KEY (`favourite_ID_article`) REFERENCES `articles` (`ID_article`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_users_has_articles_users2` FOREIGN KEY (`favourite_ID_user`) REFERENCES `users` (`ID_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
    ADD CONSTRAINT `reports_ibfk_1` FOREIGN KEY (`user_report`) REFERENCES `users` (`ID_user`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `reports_ibfk_2` FOREIGN KEY (`comment_report`) REFERENCES `comments` (`ID_comment`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
    ADD CONSTRAINT `fk_user_role1` FOREIGN KEY (`user_ID_role`) REFERENCES `roles` (`ID_role`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`thread_ID_user`) REFERENCES `comment_threads` (`ID_thread`);
COMMIT;
